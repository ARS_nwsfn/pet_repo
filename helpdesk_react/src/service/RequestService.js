import React, {useEffect } from 'react';
import {
  Link,
  useHistory
} from "react-router-dom";
export async function useGetRequest(url){

    const history = useHistory();
    const res = await fetch(url);
    if (res.status === 403){
        history.push("/login");
    }
    return await res.json();
}