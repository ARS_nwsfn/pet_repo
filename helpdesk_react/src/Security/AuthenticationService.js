export default class Auth {

  constructor() {
    this.email = '';
    this.role = '';
  }

  static getEmail() {
    return this.email;
  }
  static getRole() {
    return this.role
  }

  getAuth() {
    fetch("/api/user/getAuth").then(result => result.json()).then(result => {
      const {email, role} = result
      this.email = email
      this.role = role
    })
  }
}
