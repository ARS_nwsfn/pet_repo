export const ticketSelector = (state) => state.ticketList.tickets

export const fileSelector = (state) => state.fileList.files
export const ownershipSelector = (state) => state.fileList.ownership

export const ticketIdSelector = (state) => state.ticketInfo.ticketId
export const historyOrCommentsSelector = (state) => state.ticketInfo.historyOrComments
export const amountOfHistoryOrCommentsSelector = (state) => state.ticketInfo.amountOfHistoryOrComments