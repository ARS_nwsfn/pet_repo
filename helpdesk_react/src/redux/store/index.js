import reducer from "../reduser/index";
import { createStore } from "redux";

const store = createStore(reducer);

export default store;

window.store = store;