const ADD_TICKETS = 'ADD_TICKETS';

export const addTickets = payload => ({
  type: ADD_TICKETS,
  payload
})

const defaultState = {
  tickets: [],
  loading: false,
  loaded: false
}

export default (state = defaultState, action) =>{
    const { type, payload } = action;
    switch (type) {
        case ADD_TICKETS:
          return {...state, tickets: payload}
        default:
          return state;
      }
}