const ADD_FILES = 'ADD_FILES';
const CHANGE_OWNERSHIP = 'CHANGE_OWNERSHIP';
const CHANGE_TICKET_ID = 'CHANGE_TICKET_ID';

export const addFiles = payload => ({
  type: ADD_FILES,
  payload
})

export const changeOwnership = payload => ({
  type: CHANGE_OWNERSHIP,
  payload
})

export const changeTicketId = payload => ({
  type: CHANGE_TICKET_ID,
  payload
})

const defaultState = {
  files: [],
  ownership: false,
  ticketId: null,
  loading: false,
  loaded: false
}

export default (state = defaultState, action) =>{
    const { type, payload } = action;
    switch (type) {
        case ADD_FILES:
          return {...state, files: payload}
        case CHANGE_OWNERSHIP:
          return {...state, ownership: payload}
        case CHANGE_TICKET_ID:
          return {...state, ticketId: payload}
        default:
          return state;
      }
}