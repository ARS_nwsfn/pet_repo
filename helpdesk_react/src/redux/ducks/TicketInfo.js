const CHANGE_TICKET_ID = 'CHANGE_TICKET_ID';
const SWITCH_HISTORY_COMMENTS = 'SWITCH_HISTORY_COMMENTS';
const CHANGE_AMOUNT_OF_HISTORY_OR_COMMENTS = 'CHANGE_AMOUNT_OF_HISTORY_OR_COMMENTS';

export const changeTicketId = payload => ({
  type: CHANGE_TICKET_ID,
  payload
})

export const switchHistoryComments = payload => ({
  type: SWITCH_HISTORY_COMMENTS,
  payload
})

export const changeAmountOfHistoryOrComments = payload => ({
  type: CHANGE_AMOUNT_OF_HISTORY_OR_COMMENTS,
  payload
})

const defaultState = {

  ticketId: null,
  historyOrComments: true,
  amountOfHistoryOrComments: 5
}

export default (state = defaultState, action) =>{
    const { type, payload } = action;
    switch (type) {
        case CHANGE_TICKET_ID:
          return {...state, ticketId: payload}
        case SWITCH_HISTORY_COMMENTS:
          return {...state, historyOrComments: payload}
        case CHANGE_AMOUNT_OF_HISTORY_OR_COMMENTS:
          return {...state,amountOfHistoryOrComments: payload}
        default:
          return state;
      }
}