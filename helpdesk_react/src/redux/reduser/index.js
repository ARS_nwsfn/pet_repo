import { combineReducers } from "redux";
import ticketList from "../ducks/TicketList";
import fileList from "../ducks/FileList";
import ticketInfo from "../ducks/TicketInfo";

export default combineReducers({
  ticketList,
  fileList,
  ticketInfo
});