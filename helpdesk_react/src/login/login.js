import React, { useState } from 'react';
export default function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const handleLoginChange = value => setEmail(value);
  const handlePasswordChange = value => setPassword(value);
  const params = new URLSearchParams(window.location.search);
  return (
  <>
    <div className="container">
        <header>
            <h1>Login</h1>
        </header>
        {params.get('error') &&
        <div className="alert alert-error">
                <div>
                    <strong>Please make sure you are using a valid email or password</strong>
                </div>
         </div>
         }
         {params.get('logout') &&
                 <div className="alert alert-error">
                         <div>
                             <strong>Okay, Houston, you are logged out successfully .</strong>
                         </div>
                  </div>
                  }
    <form className="form-horizontal" action="/login" method="POST">
        <fieldset>
                    <div className="control-group">
                        <label className="control-label">Login</label>
                        <div className="controls">
                            <div className="input-prepend">
                                <span className="add-on">@</span>
                                <input id="loginField" name="email" className="span3" type="email" value={email} onChange={e => handleLoginChange(e.target.value)}/>
                            </div>
                        </div>
                    </div>
                    <div className="control-group">
                        <label className="control-label">Password</label>
                        <div className="controls">
                            <input id="passwordField" name="password" className="span3" type="password" value={password} onChange={e => handlePasswordChange(e.target.value)}/>
                        </div>
                    </div>
                    <div className="form-actions">
                        <button id="loginButton" className="btn btn-primary" type="submit">Login</button>
                    </div>
                </fieldset>
        </form>
        </div>
        </>
  );
}