import React, { useState, useEffect } from 'react';
import {useGetRequest} from '../service/RequestService';
export default function Header() {

    const [email, setEmail] = useState("");
    useGetRequest("/api/user/getAuth").then(res => setEmail(res.email));

  return (
  <>
    <header>
            <h1 align="center">
                {email && email !== "anonymousUser" &&
                <div className="logout">
                            <span id="currentUserLogin">
                                {email}
                            </span>
                    <a href="http://localhost:8080/logout">
                        <em className="icon-off"></em>
                    </a>
                </div>
                }
            </h1>
    </header>
    <div className="pre-post-space"></div>
  </>
  );
}