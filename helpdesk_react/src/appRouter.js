import React from "react";
import Login from "./login/login";
import TicketList from "./ticket/ticketList/TicketList";
import CreateTicket from "./ticket/createticket/CreateTicket";
import SpecificTicket from "./ticket/specificticket/SpecificTicket";
import Feedback from "./ticket/feedback/Feedback";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function BasicExample() {
  return (
    <Router>
      <div>
        <hr />
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
           <Route path="/login">
            <Login />
           </Route>
          <Route path="/tickets">
            <TicketList />
          </Route>
          <Route path="/ticket/create">
           <CreateTicket />
          </Route>
          <Route path="/ticket/:ticketId/edit" component={CreateTicket}>
          </Route>
          <Route path="/ticket/:ticketId/feedback" component={Feedback}>
          </Route>
          <Route path="/ticket/:ticketId" component={SpecificTicket}>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

{/*
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/login">Log in here</Link>
          </li>
          <li>
            <Link to="/tickets">Tickets</Link>
          </li>
          <li>
            <Link to="/ticket/create">Create Ticket</Link>
          </li>
        </ul>
         */}