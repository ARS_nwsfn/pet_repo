import React,{ useState, useEffect, useRef } from "react";
import Header from "../../templates/header";
import {
  Link
} from "react-router-dom";
import { useHistory } from "react-router-dom";

export default function CreateTicket({match}) {

    const [categories, setCategories] = useState([]);
    const [urgencies, setUrgencies] = useState([]);
    const [allErrors, setAllErrors] = useState([]);
    const [category, setCategory] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [urgency, setUrgency] = useState("");
    const [desiredDate, setDesiredDate] = useState("");
    const [files, setFiles] = useState("");
    const [id, setId] = useState(null);
    useEffect(() => {
    if (match) {
        setId(match.params.ticketId);
        const getTicket = async (url) => {
        const res = await fetch(url);
        return await res;
        }
        getTicket("/api/ticket/" + match.params.ticketId + "/edit").then(res => res.json().then(data =>
        {
            if (res.status === 200){
                setName(data.name);
                setCategory(data.category);
                if(data.description) setDescription(data.description);
                setUrgency(data.urgency);
                if(data.desiredResolutionDate){
                    let date = new Date(data.desiredResolutionDate);
                    let month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                    let day = date.getDate() < 10 ? "0" + (date.getDate() + 1) : date.getDate() + 1;
                    let dateString = date.getFullYear() + "-" + month + "-" + day;
                    setDesiredDate(dateString);
                }
                if(data.file) setFiles(data.file);
             }
            }));
    }
    }, [])
    const fileInputRef = useRef();
    const dateInputRef = useRef();
    const formRef = useRef();
    const history = useHistory();
    useEffect(() => {
        const getOptions = async (url) => {
        const res = await fetch(url)
        return await res.json();
        }
        getOptions("/api/ticket/getCategories").then(res =>
        {   setCategories(res.options);
            });
        getOptions("/api/ticket/getUrgencies").then(res =>
        {   setUrgencies(res.options);
            });
        }, [])

    useEffect(() => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        today = yyyy + '-' + mm + '-' + dd;
            dateInputRef.current.min = today;
            }, [])

    function clearInput(event) {
            event.preventDefault();
            fileInputRef.current.value = "";
            setFiles("");
     }

  async function handleSubmit(event,action) {
     event.preventDefault();
     let url ="";
     if (id){
        url = "/api/ticket/" + id + "/edit?status=" + action;
     } else {
        url = "/api/ticket/create?status=" + action;
        }
     const formData = new FormData(formRef.current);
     try {
                const response = await fetch(url, {
                  method: 'POST',
                  body: formData
                });
                const result = await response.json();
                setAllErrors(result.fieldErrors);
                 if (response.status === 200){
                      history.push("/ticket/" + result.ticketId);
                 };
      } catch (error) {
           console.error('error:', error);
      }

  }
  return (
  <>
    <div className="container">
       <Header/>
       <form method="POST" encType="multipart/form-data" ref={formRef}>
               <table>

                   <tbody>
                   <tr>
                       <td>
                        <Link to="/tickets" className="button-green button-green-to-white">
                             Ticket List
                        </Link>
                       </td>
                   </tr>

                   {/* <!--Category--> */}
                   <tr>
                       <td className="first-column"><h5> Category <span className="red-star">*</span></h5></td>
                       <td className="second-column">
                            {allErrors && Object.keys(allErrors).map(key =>
                             key.includes("category") ? (
                                <div className="alert alert-error" key={allErrors[key]}>
                                    <div>
                                         <strong>{allErrors[key]}</strong>
                                    </div>
                                </div>) : null
                            )}
                           <select name="category" style={{width:284}} onChange={e => setCategory(e.target.value)} value={category}>
                               {categories && categories.map(category =>
                                    <option key={category}> {category} </option>
                                )}
                           </select>
                       </td>
                   </tr>

                   {/* <!--Name--> */}
                   <tr>
                       <td className="first-column"><h5> Name<span className="red-star">*</span></h5></td>
                       <td className="second-column">
                            {allErrors && Object.keys(allErrors).map(key =>
                             key.includes("name") ? (
                                <div className="alert alert-error" key={allErrors[key]}>
                                    <div>
                                         <strong>{allErrors[key]}</strong>
                                    </div>
                                </div>) : null
                            )}
                           <input type="text" className="input-text" name="name" id="name" placeholder="enter name" value={name} onChange={e => setName(e.target.value)}/>
                       </td>
                   </tr>

                   {/* <!--Description--> */}
                   <tr>
                       <td className="first-column"><h5> Description </h5></td>
                       <td className="second-column">
                             {allErrors && Object.keys(allErrors).map(key =>
                                key.includes("description") ? (
                                 <div className="alert alert-error" key={allErrors[key]}>
                                     <div>
                                          <strong>{allErrors[key]}</strong>
                                     </div>
                                 </div>) : null
                             )}
                           <input type="text" className="input-description"
                                  placeholder="enter description" name="description" onChange={e => setDescription(e.target.value)} value={description}/>
                       </td>
                   </tr>

                   {/* <!--Urgency--> */}
                   <tr>
                       <td className="first-column"><h5> Urgency<span className="red-star">*</span></h5></td>
                       <td className="second-column">
                             {allErrors && Object.keys(allErrors).map(key =>
                              key.includes("urgency") ? (
                                 <div className="alert alert-error" key={allErrors[key]}>
                                     <div>
                                          <strong>{allErrors[key]}</strong>
                                     </div>
                                 </div>) : null
                             )}
                           <select name="urgency" className="input-text" onChange={e => setUrgency(e.target.value)} value={urgency}>
                               {urgencies && urgencies.map(urgency =>
                                     <option key={urgency}> {urgency} </option>
                                )}
                           </select>
                       </td>
                   </tr>

                   {/* <!--resolution date--> */}
                   <tr>
                       <td className="first-column"><h5> Desired resolution date </h5></td>
                       <td className="second-column">
                             {allErrors && Object.keys(allErrors).map(key =>
                                key.includes("date") ? (
                                 <div className="alert alert-error" key={allErrors[key]}>
                                     <div>
                                          <strong>{allErrors[key]}</strong>
                                     </div>
                                 </div>) : null
                             )}
                           <input type="date" name="desiredResolutionDate" id="desiredDate" ref={dateInputRef} onChange={e => setDesiredDate(e.target.value)} value={desiredDate}/>
                       </td>
                   </tr>
                   {/* <!--Attachment--> */}
                   <tr>
                       <td className="first-column"><h5>Add Attachments </h5></td>
                       <td className="second-column">
                             {allErrors && Object.keys(allErrors).map(key =>
                                key.includes("file") ? (
                                 <div className="alert alert-error" key={allErrors[key]}>
                                     <div>
                                          <strong>{allErrors[key]}</strong>
                                     </div>
                                 </div>) : null
                             )}
                           <input type="file" className="input-file" name="file" id="file"
                                  accept=".pdf, .doc, .docx, .png, .jpeg, .jpg, .txt" ref={fileInputRef} onChange={e => setFiles(e.target.value)} value={files} multiple/>
                                   <br/>{files && <button id="remove-attachment" onClick={e => {clearInput(e)}}>remove</button>}
                       </td>
                   </tr>

                   {/* <!--Comments--> */}
                   <tr>
                       <td className="first-column"><h5> Comments </h5></td>
                       <td className="second-column">
                             {allErrors && Object.keys(allErrors).map(key =>
                                key.includes("comment") ? (
                                 <div className="alert alert-error" key={allErrors[key]}>
                                     <div>
                                          <strong>{allErrors[key]}</strong>
                                     </div>
                                 </div>) : null
                             )}
                           <input type="text" className="input-comment" placeholder="enter comment" name="comment"/>
                       </td>
                   </tr>
                   </tbody>
               </table>
               <div className="flex-space-container">
                   <button className="button-grey button-grey-to-white" type="submit" name="draft" value="draft" onClick={e => {handleSubmit(e,"draft")}} >Save As Draft
                   </button>
                   <button className="button-green button-green-to-white" type="submit" name="submit" value="submit"  onClick={e => {handleSubmit(e,"submit")}} >Submit
                   </button>
               </div>
           </form>
    </div>
  </>
  );
}
