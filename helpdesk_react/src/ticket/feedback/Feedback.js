import React, { useState, useEffect, useRef} from 'react';
export default function Feedback({match}) {

    const [ticketId, setTicketId] = useState(match.params.ticketId);
    const [rate, setRate] = useState(0);
    const [feedbackProvided, setFeedbackProvided] = useState(false);
    const [feedback, setFeedback] = useState("");
    const [ticketName, setTicketName] = useState("");
    const [feedbackText, setFeeedbackText] = useState("");
    const commentInputRef = useRef();

    useEffect(() => {
    const getFeedback = async (url) => {
    const res = await fetch(url)
    return await res.json();
    }
    getFeedback("/api/ticket/" + ticketId +"/feedback").then(res => {
    if (res.hasFeedback){
        setRate(res.feedback.rate);
        setFeedbackProvided(true);
        setFeedback(res.feedback);
        setTicketName(res.ticketName);
        setFeeedbackText(res.feedback.text);
    } else {
        setFeedbackProvided(false);
    }
    });
    }, [])

    function changeStarRating(rating){
    if (feedbackProvided) return;
        setRate(rating);
    }

    async function submitFeedback(){
    let comment = commentInputRef.current.value;
    setFeeedbackText(comment);
    let rating = rate;
    if (!comment){ return }
        let requestBody = {
          comment,
          rating
        }

        let result = await fetch("/api/ticket/" + ticketId + "/provide-feedback", {
          method: "POST",
          body: JSON.stringify(requestBody),
          headers: {
                  'Content-Type': 'application/json',
                }
        })
        if (result.status === 200){
            setFeedbackProvided(true);
      }
    }

  return (
  <>
    <div className="container" style={{marginTop: 100}}>
        <div className="container flex-cont">
            <a className="button_simple button-simple-white"> Back </a>
            <p className="h13"> Ticket (<span >2</span>) - <span>Task 2</span>
            </p>
        </div>
        <div className="container" style={{marginTop:50}}>
            {feedbackProvided &&
            <p className="h13">feedback:</p>
            }
            {!feedbackProvided &&
            <p className="h13">Please, rate your satisfaction with the solution:</p>
            }
        </div>
        <div  style={{marginTop: 50}}>
            <svg className={`star ${ rate >= 1 ? 'star__selected': ''}`} onClick={e => changeStarRating(1)}>
                <polygon points="20,2 8,40 38,16 2,16 32,40"/>
            </svg>
            <svg className={`star ${ rate >= 2 ? 'star__selected': ''}`} onClick={e => changeStarRating(2)}>
                <polygon points="20,2 8,40 38,16 2,16 32,40"/>
            </svg>
            <svg className={`star ${ rate >= 3 ? 'star__selected': ''}`} onClick={e => changeStarRating(3)}>
                <polygon points="20,2 8,40 38,16 2,16 32,40"/>
            </svg>
            <svg className={`star ${ rate >= 4 ? 'star__selected': ''}`} onClick={e => changeStarRating(4)}>
                <polygon points="20,2 8,40 38,16 2,16 32,40"/>
            </svg>
            <svg className={`star ${ rate >= 5 ? 'star__selected': ''}`} onClick={e => changeStarRating(5)}>
                <polygon points="20,2 8,40 38,16 2,16 32,40"/>
            </svg>

        </div>

        <div className="flex-container">
            {!feedbackProvided &&
            <>
            <input type="text" id="add-comment-textarea" className="textarea"  ref={commentInputRef}/>
            <button className="button_simple button-simple-white" id="submit-feedback" onClick={e => submitFeedback()}>
                Submit </button>
            </>
            }
            {feedbackProvided &&
            <input type="text" id="comment-textarea" className="textarea" value={feedbackText} readOnly />
            }

        </div>
    </div>
  </>
  );
}