import React, { useEffect } from 'react';
import { connect } from "react-redux";
import {ticketSelector} from '../../redux/selectors/';
import {addTickets} from '../../redux/ducks/TicketList';
import {
  Link
} from "react-router-dom";

function TicketTable({tickets, addTickets}) {

    useEffect(() => {
    const getTickets = async (url) => {
    const res = await fetch(url)
    return await res.json();
    }
    getTickets("/api/ticket/getAll?orderBy=urgency&orderType=asc").then(res => addTickets(res.tickets));
    }, [])

    function getFullDate(dateString) {
            let date = new Date(dateString);
            return date.toLocaleDateString();
    }

    async function changeState(ticketId, action) {
        let requestBody = {
           action
         }

         let result = await fetch("/api/ticket/" + ticketId + '/change_state', {
           method: "PUT",
           body: JSON.stringify(requestBody),
           headers: {
                   'Content-Type': 'application/json',
                 }
         })
         if (result.status === 200){
                      const reloadTicket = async (url) => {
                      const res = await fetch(url)
                      return await res.json();
                      }
                      var url = "/api/ticket/getAll?task=all&";
                      reloadTicket(url).then(res => addTickets(res.tickets));
       }
    }

    const body = tickets && (<tbody>{tickets.map(ticket =>
            <tr  key={ticket.id}>
                <td> {ticket.id}</td>
                <td>
                    <Link to={`/ticket/${ticket.id}`}>{ticket.name}</Link>
                </td>
                <td> {getFullDate(ticket.desiredResolutionDate)}</td>
                <td>{ticket.urgency}</td>
                <td>{ticket.state}</td>
                <td>
                {ticket.actions.length > 0 &&
                    <div className="dropdown">
                        <button className="btn-success">action</button>
                        <div id="dropdown-content" className="dropdown-content" >
                            {ticket.actions.map(action =>
                            <div key={action}>
                                <button className="btn-add" onClick={() => {changeState(ticket.id, action)}}>{action}</button>
                            </div>
                            )}
                        </div>
                    </div>
                }
                </td>
            </tr>
    )}</tbody>)
  if(!body) return null;
  return (
  <>
    <table className="table table-striped table-bordered">
        <thead>
             <tr>
                    <th className="span1" data-column_number="id">id <div className="image-order-default"/>
                    </th>
                    <th className="span1" data-column_number="name">name <div className="image-order-default"/>
                    </th>
                    <th className="span1" data-column_number="date">Desired Date <div className="image-order-default"/>
                    </th>
                    <th className="span1" data-column_number="urgency">Urgency<div className="image-order-default"/>
                    </th>
                    <th className="span1" data-column_number="state">Status <div className="image-order-default"/>
                    </th>
                    <th className="span1">Action</th>
             </tr>
        </thead>
        {body}
    </table>
  </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  tickets: ticketSelector(state)
})

const mapDispatchToProps = {
  addTickets
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketTable);