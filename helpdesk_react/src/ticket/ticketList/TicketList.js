import React, { useState, useEffect } from 'react';
import Header from "../../templates/header";
import TicketTable from "./TicketTable";
import { connect } from "react-redux";
import {ticketSelector} from '../../redux/selectors/';
import {addTickets} from '../../redux/ducks/TicketList';
import {
  Link
} from "react-router-dom";
import {useGetRequest} from '../../service/RequestService';

function TicketList({tickets, addTickets}) {

    const [currentFilter, setFilter] = useState("");
    const [currentOrder, setOrder] = useState("urgency");
    const [currentOrderType, setOrderType] = useState("asc");
    const [currentTask, setTask] = useState("all");
    const [email, setEmail] = useState("");
    const [role, setRole] = useState("");
    useGetRequest("/api/user/getAuth").then(res =>
        {   setEmail(res.email);
            setRole(res.role);
        });
    useEffect(() => {
        const getTicket = async (url) => {
            const res = await fetch(url);
            return await res.json();
            };
        var url = "/api/ticket/getAll?task=" + currentTask + "&";
        if (currentFilter && currentFilter !== "none") url = url + "filter=" + currentFilter + "&";
        if (currentOrder && currentOrderType) url = url + "orderBy=" + currentOrder + "&orderType=" + currentOrderType;
        getTicket(url).then(res => addTickets(res.tickets));
    }, [currentFilter, currentOrder, currentOrderType, currentTask])

  return (
  <>

    <div className="container">
        <Header />
        {email && role !== "ENGINEER" &&
        <div >
                <Link to="/ticket/create" className="button-green button-green-to-white" >
                    Create New Ticket
                </Link>
        </div>
        }
        <div className="pre-post-space"></div>
        <div className="flex-container">
            <button id="all-tickets" className="button-blue button-blue-to-white" onClick={() => {setOrder("urgency"); setOrderType("asc"); setFilter("none"); setTask("all")}}>
                    All Tickets
            </button>
            {email && role !== "ENGINEER" &&
                <button id="my-tickets" className="button-blue button-blue-to-white" onClick={() => {setOrder("urgency");setOrderType("asc"); setFilter("none"); setTask("my")}}>
                    My Tickets
                </button>
            }
        </div>
        {/* manager buttons */}
        {email && role === "MANAGER" &&
            <div className="flex-space-container">
                <button className="button-blue button-blue-to-white" id="new-tickets">
                    all new
                </button>
                <a className="button-blue button-blue-to-white" id="manager-tickets" href="/api/ticket/manager">
                    Approved by me
                </a>
            </div>
        }
        {/* ENGINEER buttons */}
        {email && role === "ENGINEER" &&
            <div className="flex-space-container">
                <a className="button-blue button-blue-to-white" id="approved-tickets" href="/api/ticket/approved">
                    all approved
                </a>
                <a className="button-blue button-blue-to-white" id="assigned-tickets" href="/api/ticket/assign">
                    for me in progress and done
                </a>
            </div>
        }
        <div className="pre-post-space"></div>
        <div className="flex-container">
                <input type="text" className="search-input" onChange={e => setFilter(e.target.value)}/>
        </div>
        <TicketTable />
    </div>
  </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  tickets: ticketSelector(state)
})

const mapDispatchToProps = {
  addTickets
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketList);

