import React, { useState, useEffect, useRef} from 'react';
import Header from "../../templates/header";
import {
  Link,
  useHistory
} from "react-router-dom";
import FileList from "./FileList";
import History from "./historyFragment";
import Comments from "./commentsFragment";
import { connect } from "react-redux";
import {fileSelector, ownershipSelector, ticketIdSelector, historyOrCommentsSelector, amountOfHistoryOrCommentsSelector} from '../../redux/selectors/';
import {addFiles, changeOwnership} from '../../redux/ducks/FileList';
import {changeTicketId, switchHistoryComments, changeAmountOfHistoryOrComments} from '../../redux/ducks/TicketInfo';

function SpecificTicket({files, ownership, ticketId, historyOrComments, amountOfHistoryOrComments,
                        addFiles, changeOwnership, changeTicketId, switchHistoryComments,changeAmountOfHistoryOrComments,
                         match}) {

    changeTicketId(match.params.ticketId);
    const history = useHistory();
    const [alertMsg, setAlertMsg] = useState("");
    const commentInputRef = useRef();

    const [feedback, setFeedback] = useState(false);
    const [category, setCategory] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [urgency, setUrgency] = useState("");
    const [desiredDate, setDesiredDate] = useState("");
    const [createdDate, setCreatedDate] = useState("");
    const [status, setStatus] = useState("");
    const [owner, setOwner] = useState("");
    const [approver, setApprover] = useState("");
    const [assignee, setAssignee] = useState("");
    useEffect(() => {
    const getTicket = async (url) => {
    const res = await fetch(url);
    return await res;
    }
    getTicket("/api/ticket/" + match.params.ticketId).then(res => res.json().then(data =>
    {
        if (res.status === 200){
            setName(data.ticket.name);
            changeOwnership(data.isOwner);
            setFeedback(data.feedback);
            setCategory(data.ticket.category);
            setStatus(data.ticket.state);
            setOwner(data.ticket.owner);
            setApprover(data.ticket.approver);
            setAssignee(data.ticket.assignee);
            setDescription(data.ticket.description);
            if(data.ticket.description) setDescription(data.ticket.description);
            setUrgency(data.ticket.urgency);
            if(data.ticket.desiredResolutionDate){
                let date = new Date(data.ticket.desiredResolutionDate);
                let month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                let day = date.getDate() < 10 ? "0" + (date.getDate() + 1) : date.getDate() + 1;
                let dateString = date.getFullYear() + "-" + month + "-" + day;
                setDesiredDate(dateString);
            }
            if(data.ticket.createdOn){
                let date = new Date(data.ticket.createdOn);
                let month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
                let day = date.getDate() < 10 ? "0" + (date.getDate() + 1) : date.getDate() + 1;
                let dateString = date.getFullYear() + "-" + month + "-" + day;
                setCreatedDate(dateString);
            }
            getAttachments();

        } else {
            history.push("/tickets");
        }
    }));
    }, [])

    async function getAttachments(){
        await fetch("/api/file/getByTicket/" + match.params.ticketId)
            .then(fileRes => fileRes.json()
            .then(fileData =>
             {
                if (fileRes.status === 200){
                    addFiles(fileData.attachments);
                } else addFiles([]);
                }
            ))
    }

    function changeAmount(event){
        event.preventDefault();
        changeAmountOfHistoryOrComments(0);
    }

async function createComment() {

    let comment = commentInputRef.current.value;
    let requestBody = {
      comment
    }

    let result = await fetch("/api/ticket/" + ticketId + "/add-comment", {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
              'Content-Type': 'application/json',
            }
    })
    if (result.status == 200){
        changeAmountOfHistoryOrComments(5);
        setAlertMsg('');
        commentInputRef.current.value = '';
        } else {
            setAlertMsg('comment not saved. Comment should not be longer than 500 symbols and allowed to enter only lowercase English alpha characters, digits and special characters');
        }
  }

  return (
  <>
    <div className="container">
        <Header/>
        <div className="flex-container">
            <Link to="/tickets" className="button-green button-green-to-white button-medium">
                Ticket List
            </Link>
            <h3 style={{paddingLeft:70}}> Ticket (<span>{ticketId}</span>) - <span>{name}</span>
            </h3>
            <div style={{paddingLeft:125}}>
            {ownership && (status === "NEW" || status === "DRAFT") &&
            <Link to={`/ticket/${ticketId}/edit`} className="button-green button-green-to-white button-medium">
                Edit
            </Link>
            }
            </div>
        </div>
        <div>
            {status === "DONE" && ownership && !feedback &&
            <Link to={`/ticket/${ticketId}/feedback`}
                className="button-green button-green-to-white button-medium" style={{padding:10, height:40}}>
                    Provide Feedback
            </Link>
            }
            {status === "DONE" && feedback &&
            <Link to={`/ticket/${ticketId}/feedback`}
                className="button-green button-green-to-white button-medium" style={{padding:10, height:40}}>
                Feedback
            </Link>
             }
        </div>
            <div className="container">
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    <tr>
                        <td className="column-first-overiview">created on</td>
                        <td className="column-second-overiview">
                            {createdDate}
                        </td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Status</td>
                        <td className="column-second-overiview"><span> {status} </span> <span
                                className="text-right" style={{fontWeight: 'bold'}}>Category: </span>
                            <span className="text-right"> {category}</span></td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Urgency</td>
                        <td className="column-second-overiview" >{urgency}</td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Desired resolution date</td>
                        <td className="column-second-overiview">{desiredDate}</td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Owner</td>
                        <td className="column-second-overiview">{owner}</td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Approver</td>
                        <td className="column-second-overiview" >{approver}</td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Assignee</td>
                        <td className="column-second-overiview" >{assignee}</td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Attachments</td>
                        <td className="column-second-overiview">
                            <div>
                            <FileList />
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td className="column-first-overiview">Description</td>
                        <td className="column-second-overiview textarea">{description}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div className="flex-space-container">
                    <button  id="get-history"
                       className="button-blue button-blue-to-white" onClick={e => {switchHistoryComments(true); changeAmountOfHistoryOrComments(5);}}>
                        History
                    </button>
                    <button  id="get-comments"
                       className="button-blue button-blue-to-white" onClick={e => {switchHistoryComments(false); changeAmountOfHistoryOrComments(5);}}>
                        Comments
                    </button>
            </div>
            <div className="flex-container">
                <a href="" id="get-all" onClick={e => changeAmount(e)}>
                 Get All
                </a>
            </div>
            <div className="flex-container">
                {historyOrComments &&
                    <History/>
                }
                {!historyOrComments &&
                    <Comments/>
                }
            </div>
            <div className="container">
                <div id="commentAlertMsg" style={{color:'red', fontSize:18}}>{alertMsg}</div>
                <input type="text" id="add-comment-textarea" className="textarea" ref={commentInputRef}/> <a
                    className="button-grey button-grey-to-white button-medium" id="add-comment-btn" align="left" onClick={e => createComment()}> Add comment </a>
            </div>
    </div>
  </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  files: fileSelector(state),
  ownership: ownershipSelector(state),
  ticketId: ticketIdSelector(state),
  historyOrComments: historyOrCommentsSelector(state),
  amountOfHistoryOrComments: amountOfHistoryOrCommentsSelector(state)
})

const mapDispatchToProps = {
  addFiles,
  changeOwnership,
  changeTicketId,
  switchHistoryComments,
  changeAmountOfHistoryOrComments
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecificTicket);