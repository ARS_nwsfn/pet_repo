import React, {useState , useRef} from 'react';
import { connect } from "react-redux";
import {fileSelector,ownershipSelector, ticketIdSelector} from '../../redux/selectors/';
import {addFiles,changeOwnership} from '../../redux/ducks/FileList';
import {changeTicketId} from '../../redux/ducks/TicketInfo';
import {
  Link
} from "react-router-dom";

function FileList({files, ownership, ticketId, addFiles, changeOwnership, changeTicketId}) {

    const fileInputRef = useRef();
    const fileUploadRef = useRef();
    const [alertMsg, setAlertMsg] = useState("");

    async function downloadAttachment(event, fileId) {
        event.preventDefault();
        let url1 = '/api/file/' + fileId + '/download';
        const response = await fetch(url1);
        const headers = response.headers;
        const file = headers.get("Content-Disposition");
        let fileName ="";
        if (file) fileName = file.slice(file.indexOf("=\"") + 2, file.length-1);
        const blob = response.blob();
        const url = window.URL.createObjectURL(new Blob([blob]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
    }

    async function getAttachments(){
        await fetch("/api/file/getByTicket/" + ticketId)
            .then(fileRes => fileRes.json()
            .then(fileData =>
             {
                if (fileRes.status === 200){
                    addFiles(fileData.attachments);
                } else addFiles([]);
                }
            ))
    }

        function removeAttachment(event, fileId){
             event.preventDefault();
             var href = '/api/file/' + fileId + '/ticket/' + ticketId;
             requestDeleteAttachment("DELETE",href, ticketId);
        }

        async function requestDeleteAttachment(method, url, ticketId) {
          const response = await fetch(url, {
            method
          })
            getAttachments();
            {/*loadHistoryOrComments(1, ticketId, 5);*/}
            setAlertMsg('');
        return response.status
        }

    function clearInput(event) {
        event.preventDefault();
        setAlertMsg("");
    }

    async function uploadFile(event){
    event.preventDefault();
    const url = "/api/file/" + ticketId + "/upload";
    const formData = new FormData(fileUploadRef.current);
    try {
                const response = await fetch(url, {
                  method: 'POST',
                  body: formData
                });
                const result = await response.json();
                 if (response.status === 200){
                    setAlertMsg('');
                    getAttachments();
                 };
      } catch (error) {
           setAlertMsg('file upload Error');
      }
    }

    const body = files && (<table>
            <tbody>
            {(files.map(file =>
            <tr key={file.id}>
                <td className="span1"> {file.name}</td>
                <td className="span1">
                    <button
                       className="btn simple-button"
                       data-action="downloadAttachment" onClick={e => {downloadAttachment(e, file.id)}}>
                        download
                    </button>
                </td>
                <td className="span1">
                    {ownership &&
                    <button className="btn simple-button" data-action="deleteAttachment" onClick={e => {removeAttachment(e, file.id)}}>
                        delete
                    </button>
                    }
                </td>
            </tr>
            ))}
            </tbody>
     </table>)

  if(!body) return null;
  return (
  <>
     {body}
     {ownership &&
        <form method="post" encType="multipart/form-data" ref={fileUploadRef}>
        <input className="form-control" type="file" name="file" id="file" ref={fileInputRef}/>
        <button className="btn simple-button" id="upload-file" type="submit" onClick={e => {uploadFile(e)}}>
              Upload
        </button> <button id="remove-attachment" className="btn simple-button" onClick={e => {clearInput(e)}}>remove</button>
        </form>
      }
     <div id="alertMsg" style={{color: 'red',fontSize:18}}> {alertMsg}</div>
  </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  files: fileSelector(state),
  ownership: ownershipSelector(state),
  ticketId: ticketIdSelector(state)
})

const mapDispatchToProps = {
  addFiles,
  changeOwnership,
  changeTicketId
}

export default connect(mapStateToProps, mapDispatchToProps)(FileList);