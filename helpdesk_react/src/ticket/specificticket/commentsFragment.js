import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import {ticketIdSelector, amountOfHistoryOrCommentsSelector} from '../../redux/selectors/';

function Comments({ticketId, amountOfHistoryOrComments}) {

    const [comments, setComments] = useState([]);
    useEffect(() => {
    const getComments = async (url) => {
    const res = await fetch(url)
    return await res.json();
    }
    getComments("/api/ticket/" + ticketId + "/comments?amount=" + amountOfHistoryOrComments).then(res => setComments(res.comments));
    }, [ticketId, amountOfHistoryOrComments])

    function getFullDate(dateString) {
        let date = new Date(dateString);
        return date.toLocaleDateString();
    }

  return (
  <>
        <table className="table table-striped table-bordered">
            <thead style={{backgroundColor:"#c0c0c0"}}>
            <tr>
                <th className="span1">Date
                </th>
                <th className="span1">User
                </th>
                <th className="span1">Action
                </th>
            </tr>
            </thead>
            <tbody>
            {comments.map(comment =>
            <tr key={comment.id}>
                <td className="span1">{getFullDate(comment.date)}</td>
                <td className="span1">{comment.user}</td>
                <td className="span1">{comment.text}</td>
            </tr>
            )}
            </tbody>
        </table>
  </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  ticketId: ticketIdSelector(state),
  amountOfHistoryOrComments: amountOfHistoryOrCommentsSelector(state)
})

export default connect(mapStateToProps)(Comments);