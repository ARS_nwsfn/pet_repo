import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import {ticketIdSelector, amountOfHistoryOrCommentsSelector} from '../../redux/selectors/';

function History({ticketId, amountOfHistoryOrComments}) {

    const [historyLog, setHistoryLog] = useState([]);
    useEffect(() => {
    const getHistory = async (url) => {
    const res = await fetch(url)
    return await res.json();
    }
    getHistory("/api/ticket/" + ticketId + "/history?amount=" + amountOfHistoryOrComments).then(res => setHistoryLog(res.histories));
    }, [ticketId, amountOfHistoryOrComments])

    function getFullDate(dateString) {
        let date = new Date(dateString);
        return date.toLocaleDateString();
    }

  return (
  <>
        <table className="table table-striped table-bordered">
            <thead style={{backgroundColor:"#c0c0c0"}}>
            <tr>
                <th className="span1">Date
                </th>
                <th className="span1">User
                </th>
                <th className="span1">Action
                </th>
                <th className="span1">Description
                </th>
            </tr>
            </thead>
            <tbody>
            {historyLog.map(history =>
            <tr key={history.id}>
                <td className="span1">{getFullDate(history.date)}</td>
                <td className="span1">{history.user}</td>
                <td className="span1">{history.action}</td>
                <td className="span1">{history.description}</td>
            </tr>
            )}
            </tbody>
        </table>
  </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  ticketId: ticketIdSelector(state),
  amountOfHistoryOrComments: amountOfHistoryOrCommentsSelector(state)
})

export default connect(mapStateToProps)(History);