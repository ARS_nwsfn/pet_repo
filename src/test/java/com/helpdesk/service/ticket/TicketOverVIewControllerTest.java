package com.helpdesk.service.ticket;

import com.helpdesk.model.request.AjaxCommentRequestBody;
import com.helpdesk.model.request.AjaxStateChangeRequestBody;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class TicketOverVIewControllerTest extends BaseTest {

    private static final Logger logger = Logger.getLogger(TicketOverVIewControllerTest.class);

    private AjaxCommentRequestBody comment = new AjaxCommentRequestBody();
    private AjaxStateChangeRequestBody stateChangeRequestBody = new AjaxStateChangeRequestBody();

    @Test
    public void testTicketOverview() throws Exception {

        logger.info("Test: Ticket overview page /all");
        mockMvc.perform(get("/ticket/all").with(user("user").password("123").authorities(new SimpleGrantedAuthority("MANAGER"))))
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"));

    }

    @Test
    public void testTicketAddComment() throws Exception {

        comment.setComment("");
        logger.info("Test: try saving comment");
        mockMvc.perform(post("/api/ticket/2/add-comment")
                .with(user("user").password("123").authorities(new SimpleGrantedAuthority("MANAGER")))
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(comment)))
                .andDo(print()).andExpect(status().isOk());

    }

    @Test
    public void testTicketChangeState() throws Exception {

        logger.info("Test: change state");
        stateChangeRequestBody.setAction("SUBMIT");
        mockMvc.perform(put("/api/ticket/2/change_state")
                .with(user("manager1_mogilev@yopmail.com").password("123").authorities(new SimpleGrantedAuthority("MANAGER")))
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(stateChangeRequestBody)))
                .andDo(print()).andExpect(status().isOk());

    }

}
