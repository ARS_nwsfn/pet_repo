package com.helpdesk.service.ticket;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class TicketCreateTest extends BaseTest {

    private static final Logger logger = Logger.getLogger(TicketCreateTest.class);

    @Test
    public void testTicketCreate() throws Exception {

        logger.info("Test: Create Ticket Page");
        mockMvc.perform(get("/ticket/create").with(user("user").password("123").authorities(new SimpleGrantedAuthority("MANAGER"))))
                .andExpect(status().isOk())
                .andExpect(view().name("createTicket"));

    }

    @Test
    public void testTicketCreateRestricted() throws Exception {

        logger.info("Test: Create Ticket Page restricted for engineer");
        mockMvc.perform(get("/ticket/create").with(user("user").password("123").authorities(new SimpleGrantedAuthority("ENGINEER"))))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testSaveNewTicket() throws Exception {

        MockMultipartFile file = new MockMultipartFile("file", "filename.txt", "text/plain", "some xml".getBytes());
        logger.info("Test: save new ticket");
        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/ticket/create")
                .file(file)
                .param("submit", "submit")
                .param("name", "new ticket")
                .param("description", "")
                .param("comment", "")
                .with(user("user").password("123").authorities(new SimpleGrantedAuthority("MANAGER")))
        )
                .andDo(print()).andExpect(status().isOk());

    }

    @Test
    public void testNotNewTicketEditRestricted() throws Exception {

        logger.info("Test: Edit not new Ticket");
        mockMvc.perform(get("/ticket/2/edit").with(user("manager1_mogilev@yopmail.com").password("123").authorities(new SimpleGrantedAuthority("MANAGER"))))
                .andExpect(status().isOk())
                .andExpect(view().name("error_page"));

    }

    @Test
    public void testNotOwnerTicketEditRestricted() throws Exception {

        logger.info("Test: Edit not new Ticket");
        mockMvc.perform(get("/ticket/2/edit").with(user("manager2_mogilev@yopmail.com").password("123").authorities(new SimpleGrantedAuthority("MANAGER"))))
                .andExpect(status().isOk())
                .andExpect(view().name("error_page"));

    }
}
