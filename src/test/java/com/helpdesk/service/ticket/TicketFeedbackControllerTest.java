package com.helpdesk.service.ticket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.helpdesk.model.request.AjaxFeedbackRequestbody;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class TicketFeedbackControllerTest extends  BaseTest{

    private static final Logger logger = Logger.getLogger(TicketFeedbackControllerTest.class);

    private AjaxFeedbackRequestbody feedback = new AjaxFeedbackRequestbody();

    @Test
    @WithMockUser(username = "engineer1_mogilev@yopmail.com", password = "123", roles = "ENGINEER")
    public void testDoneTicketFeedback() throws Exception {

        logger.info("Test: try feedback on ticket in DONE state");
        mockMvc.perform(get("/ticket/2/feedback"))
                .andExpect(status().isOk())
                .andExpect(view().name("feedback"));

    }

    @Test
    public void testNotDoneTicketFeedback() throws Exception {

        logger.info("Test: try feedback on ticket in NOT  DONE state");
        mockMvc.perform(get("/ticket/1/feedback"))
                .andExpect(status().isOk())
                .andExpect(view().name("error_page"));

    }

    @Test
    public void testNotDoneTicketProvideFeedback() throws Exception {

        logger.info("Test: try saving feedback on ticket in NOT DONE state");
        mockMvc.perform(post("/api/ticket/1/provide-feedback")
                .param("comment", "test")
                .param("rating", "3")
                .with(user("user").password("123").authorities(new SimpleGrantedAuthority("MANAGER")))
        ).andDo(print()).andExpect(status().is4xxClientError());

    }

    @Test
    public void testDoneTicketProvideFeedback() throws Exception {

        feedback.setComment("");
        feedback.setRating(3);
        logger.info("Test: try saving feedback on ticket in DONE state");
        mockMvc.perform(post("/api/ticket/2/provide-feedback")
                .with(user("user").password("123").authorities(new SimpleGrantedAuthority("MANAGER")))
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(feedback)))
                .andDo(print()).andExpect(status().isOk());

    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
