package com.helpdesk.validator;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class FileValidator implements Validator {

    private static final double BYTE_TO_MB_MULTIPLIER = 0.00000095367432;

    private static final List<String> ACCEPTED_EXTENSIONS = Stream.of("pdf", "doc", "docx", "png", "jpeg", "jpg")
            .collect(Collectors.toList());

    @Override
    public boolean supports(Class<?> aClass) {
        return MultipartFile.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        List<MultipartFile> files = (List<MultipartFile>) o;
        if (files.size() > 5){
            errors.rejectValue("file", "validation.file.amount", "file amount error");
        }
        for(MultipartFile file : files) {
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            if (!file.isEmpty()) {
                if (!ACCEPTED_EXTENSIONS.contains(extension)) {
                    errors.rejectValue("file", "validation.file.extension", "extension error");
                }

                if (file.getSize() * BYTE_TO_MB_MULTIPLIER > 5) {
                    errors.rejectValue("file", "validation.file.size", "size error");
                }
            }
        }
    }
}
