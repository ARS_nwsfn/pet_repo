package com.helpdesk.validator;


import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.entities.enums.State;
import com.helpdesk.jsonview.Views;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

public class TicketForm {

    @JsonView(Views.Public.class)
    private Integer id;

    @JsonView(Views.Public.class)
    @NotNull
    private String name;

    @JsonView(Views.Public.class)
    private String description;

    @JsonView(Views.Public.class)
    private String desiredResolutionDate;

    @JsonView(Views.Public.class)
    private State state;

    @JsonView(Views.Public.class)
    private String category;

    @JsonView(Views.Public.class)
    private String urgency;

    @JsonView(Views.Public.class)
    private String comment;

    @JsonView(Views.Public.class)
    private List<MultipartFile> file;

    public List<MultipartFile> getFile() {
        return file;
    }

    public void setFile(List<MultipartFile> file) {
        this.file = file;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(String desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }
}
