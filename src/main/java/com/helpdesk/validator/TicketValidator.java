package com.helpdesk.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class TicketValidator implements Validator {

    private static final Pattern VALIDATE_NAME_PATTERN = Pattern.compile("[a-z0-9$&+,:;=\\\\?@#|/'<>.^*()%!\\s]*");

    private static final Pattern VALIDATE_DESCRIPTION_COMMENT_PATTERN = Pattern.compile("[a-zA-Z0-9$&+,:;=\\\\?@#|/'<>.^*()%!\\s]*");


    @Override
    public boolean supports(Class<?> aClass) {
        return TicketForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        TicketForm ticket = (TicketForm) o;

        Matcher matcher = VALIDATE_NAME_PATTERN.matcher(ticket.getName());
        if (!matcher.matches()) {
            errors.rejectValue("name", "ticket.name.pattern", "name pattern error");
        }

        matcher = VALIDATE_DESCRIPTION_COMMENT_PATTERN.matcher(ticket.getDescription());
        if (!matcher.matches()) {
            errors.rejectValue("description", "ticket.description.pattern", "description pattern error");
        }

        matcher = VALIDATE_DESCRIPTION_COMMENT_PATTERN.matcher(ticket.getComment());
        if (!matcher.matches()) {
            errors.rejectValue("comment", "ticket.comment.pattern", "comment pattern error");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "ticket.name.required", "name required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category", "ticket.category.required", "category required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "urgency", "ticket.urgency.required", "urgency required");
        if (ticket.getName().length() > 100) {
            errors.rejectValue("name", "ticket.name.size", "Name size error");
        }
        if (ticket.getDescription().length() > 500) {
            errors.rejectValue("description", "ticket.description.size", "Description size error");
        }
        if (ticket.getComment().length() > 500) {
            errors.rejectValue("comment", "ticket.comment.size", "comment size error");
        }
    }
}
