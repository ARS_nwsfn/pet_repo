package com.helpdesk.validator;

import org.springframework.stereotype.Component;

@Component
public class TextValidator {

    public boolean validateCommentText(String text){
        boolean result = true;
        if (text.isEmpty() || text.length() > 500) {
            result = false;
        }
        return result;
    }
}
