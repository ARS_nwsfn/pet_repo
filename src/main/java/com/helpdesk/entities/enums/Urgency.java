package com.helpdesk.entities.enums;

public enum Urgency {

    CRITICAL,
    HIGH,
    AVERAGE,
    LOW
}
