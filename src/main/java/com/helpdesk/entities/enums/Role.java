package com.helpdesk.entities.enums;

public enum Role {

    EMPLOYEE,
    MANAGER,
    ENGINEER

}
