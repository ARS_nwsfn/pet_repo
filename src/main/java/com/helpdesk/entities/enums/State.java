package com.helpdesk.entities.enums;

public enum State {

    DRAFT,
    NEW,
    APPROVED,
    DECLINED,
    INPROGRESS,
    DONE,
    CANCELED
}
