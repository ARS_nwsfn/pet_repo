package com.helpdesk.entities;


import javax.persistence.*;
import java.util.Calendar;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "HISTORY")
public class History {

    @javax.persistence.Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID",
            unique = true, nullable = false)
    private Integer id;

    @Column(name = "TICKET_ID")
    private Integer ticketId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE")
    private Calendar date;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "DESCRIPTION")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
