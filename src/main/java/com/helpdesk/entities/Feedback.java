package com.helpdesk.entities;

import javax.persistence.*;

import java.util.Calendar;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "FEEDBACK")
public class Feedback {

    @javax.persistence.Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID",
            unique = true, nullable = false)
    private Integer id;

    @Column(name = "OWNER_ID")
    private Integer userId;

    @Column(name = "RATE")
    private Integer rate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE")
    private Calendar date;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "TICKET_ID")
    private Integer ticketId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}
