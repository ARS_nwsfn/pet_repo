package com.helpdesk.entities;

import com.helpdesk.entities.enums.State;
import com.helpdesk.entities.enums.Urgency;

import javax.persistence.*;

import java.util.Calendar;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "TICKET")
public class Ticket {

    @javax.persistence.Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID",
            unique = true, nullable = false)
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_ON")
    private Calendar createdOn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DESIRED_RESOLUTION_DATE")
    private Calendar desiredResolutionDate;

    @Column(name = "ASSIGNEE_ID")
    private Integer assigneeId;

    @Column(name = "OWNER_ID", nullable = false)
    private Integer ownerId;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "URGENCY")
    @Enumerated(EnumType.STRING)
    private Urgency urgency;

    @Column(name = "APPROVER_ID")
    private Integer approverId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Calendar createdOn) {
        this.createdOn = createdOn;
    }

    public Calendar getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Calendar desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }
}
