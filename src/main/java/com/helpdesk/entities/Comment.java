package com.helpdesk.entities;

import javax.persistence.*;

import java.util.Calendar;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "COMMENTS")
public class Comment {

    @javax.persistence.Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID",
            unique = true, nullable = false)
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "TEXT")
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE")
    private Calendar date;

    @Column(name = "TICKET_ID")
    private Integer ticketId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}
