package com.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

import java.util.Calendar;

public class CommentDto {

    @JsonView(Views.Public.class)
    private Integer id;

    @JsonView(Views.Public.class)
    private String user;

    @JsonView(Views.Public.class)
    private String text;

    @JsonView(Views.Public.class)
    private Calendar date;

    @JsonView(Views.Public.class)
    private Integer ticketId;



    public CommentDto(Integer id, String user, String text, Calendar date, Integer ticketId) {
        this.id = id;
        this.user = user;
        this.text = text;
        this.date = date;
        this.ticketId = ticketId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}
