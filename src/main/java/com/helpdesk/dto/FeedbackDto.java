package com.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

import java.util.Calendar;

public class FeedbackDto {

    @JsonView(Views.Public.class)
    private Integer id;

    @JsonView(Views.Public.class)
    private Integer userId;

    @JsonView(Views.Public.class)
    private String rate;

    @JsonView(Views.Public.class)
    private Calendar date;

    @JsonView(Views.Public.class)
    private String text;

    @JsonView(Views.Public.class)
    private Integer ticketId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }
}
