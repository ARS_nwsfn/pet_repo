package com.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.entities.enums.State;
import com.helpdesk.entities.enums.Urgency;
import com.helpdesk.jsonview.Views;

import java.util.Calendar;

public class SpecificTicketDto {

    @JsonView(Views.Public.class)
    private Integer id;

    @JsonView(Views.Public.class)
    private String name;

    @JsonView(Views.Public.class)
    private String description;

    private Calendar createdOn;

    @JsonView(Views.Public.class)
    private Calendar desiredResolutionDate;

    private String assignee;

    private String owner;

    private State state;

    @JsonView(Views.Public.class)
    private String category;

    @JsonView(Views.Public.class)
    private Urgency urgency;

    private String approver;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Calendar createdOn) {
        this.createdOn = createdOn;
    }

    public Calendar getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Calendar desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }
}
