package com.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.entities.enums.State;
import com.helpdesk.entities.enums.Urgency;
import com.helpdesk.jsonview.Views;

import java.util.Calendar;
import java.util.List;

public class TicketDto {

    @JsonView(Views.Public.class)
    private Integer id;

    @JsonView(Views.Public.class)
    private String name;

    @JsonView(Views.Public.class)
    private String description;

    @JsonView(Views.Public.class)
    private Calendar createdOn;

    @JsonView(Views.Public.class)
    private Calendar desiredResolutionDate;

    @JsonView(Views.Public.class)
    private Integer assigneeId;

    @JsonView(Views.Public.class)
    private Integer ownerId;

    @JsonView(Views.Public.class)
    private State state;

    @JsonView(Views.Public.class)
    private Integer categoryId;

    @JsonView(Views.Public.class)
    private Urgency urgency;

    @JsonView(Views.Public.class)
    private Integer approverId;

    @JsonView(Views.Public.class)
    private List<String> actions;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Calendar createdOn) {
        this.createdOn = createdOn;
    }

    public Calendar getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(Calendar desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public Integer getApproverId() {
        return approverId;
    }

    public void setApproverId(Integer approverId) {
        this.approverId = approverId;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }
}
