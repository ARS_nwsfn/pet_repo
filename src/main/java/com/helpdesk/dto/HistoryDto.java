package com.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

import java.util.Calendar;

public class HistoryDto {

    @JsonView(Views.Public.class)
    private Integer id;

    @JsonView(Views.Public.class)
    private Integer ticketId;

    @JsonView(Views.Public.class)
    private Calendar date;

    @JsonView(Views.Public.class)
    private String action;

    @JsonView(Views.Public.class)
    private String user;

    @JsonView(Views.Public.class)
    private String description;

    public HistoryDto(Integer id, Integer ticketId, Calendar date, String action, String user, String description) {
        this.id = id;
        this.ticketId = ticketId;
        this.date = date;
        this.action = action;
        this.user = user;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
