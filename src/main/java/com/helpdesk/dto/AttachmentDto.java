package com.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

public class AttachmentDto {

    @JsonView(Views.Public.class)
    private int id;

    @JsonView(Views.Public.class)
    private String name;

    public AttachmentDto (int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
