package com.helpdesk.model.request;

public class AjaxCommentRequestBody {

    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
