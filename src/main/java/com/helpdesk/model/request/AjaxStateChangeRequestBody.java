package com.helpdesk.model.request;

public class AjaxStateChangeRequestBody {

    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
