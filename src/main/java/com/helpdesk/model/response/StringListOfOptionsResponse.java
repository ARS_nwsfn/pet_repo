package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

import java.util.List;

public class StringListOfOptionsResponse {

    @JsonView(Views.Public.class)
    private List<String> options;

    @JsonView(Views.Public.class)
    private String code;

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
