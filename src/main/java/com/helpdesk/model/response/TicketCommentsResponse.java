package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.dto.CommentDto;
import com.helpdesk.jsonview.Views;

import java.util.List;

public class TicketCommentsResponse {

    @JsonView(Views.Public.class)
    List<CommentDto> comments;

    public List<CommentDto> getHistories() {
        return comments;
    }

    public void setComments(List<CommentDto> comments) {
        this.comments = comments;
    }
}
