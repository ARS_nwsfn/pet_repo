package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

public class SimpleMessageResponse {

    @JsonView(Views.Public.class)
    private String msg;

    @JsonView(Views.Public.class)
    private String code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AjaxResponseResult [msg=" + msg + ", code=" + code + "]";
    }

}