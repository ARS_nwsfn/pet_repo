package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.dto.TicketDto;
import com.helpdesk.jsonview.Views;

import java.util.List;

public class TicketListResponse {

    @JsonView(Views.Public.class)
    private List<TicketDto> tickets;

    @JsonView(Views.Public.class)
    private String code;

    public List<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
