package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

public class AuthResponse {

    @JsonView(Views.Public.class)
    private String email;

    @JsonView(Views.Public.class)
    private String role;

    @JsonView(Views.Public.class)
    private String code;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}