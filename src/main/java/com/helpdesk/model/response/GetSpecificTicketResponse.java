package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.dto.SpecificTicketDto;
import com.helpdesk.jsonview.Views;

public class GetSpecificTicketResponse {

    @JsonView(Views.Public.class)
    SpecificTicketDto ticket;

    @JsonView(Views.Public.class)
    Boolean isOwner;

    @JsonView(Views.Public.class)
    Boolean feedback;

    public SpecificTicketDto getTicket() {
        return ticket;
    }

    public void setTicket(SpecificTicketDto ticket) {
        this.ticket = ticket;
    }

    public Boolean getOwner() {
        return isOwner;
    }

    public void setOwner(Boolean owner) {
        isOwner = owner;
    }

    public Boolean getFeedback() {
        return feedback;
    }

    public void setFeedback(Boolean feedback) {
        this.feedback = feedback;
    }
}
