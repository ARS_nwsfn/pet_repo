package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.dto.HistoryDto;
import com.helpdesk.jsonview.Views;

import java.util.List;

public class TicketHistoryResponse {

    @JsonView(Views.Public.class)
    List<HistoryDto> histories;

    public List<HistoryDto> getHistories() {
        return histories;
    }

    public void setHistories(List<HistoryDto> histories) {
        this.histories = histories;
    }
}
