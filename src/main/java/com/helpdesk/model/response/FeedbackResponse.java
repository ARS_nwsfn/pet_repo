package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.dto.FeedbackDto;
import com.helpdesk.jsonview.Views;

public class FeedbackResponse {

    @JsonView(Views.Public.class)
    FeedbackDto feedback;

    @JsonView(Views.Public.class)
    String ticketName;

    @JsonView(Views.Public.class)
    Boolean hasFeedback;

    public FeedbackDto getFeedback() {
        return feedback;
    }

    public void setFeedback(FeedbackDto feedback) {
        this.feedback = feedback;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public Boolean getHasFeedback() {
        return hasFeedback;
    }

    public void setHasFeedback(Boolean hasFeedback) {
        this.hasFeedback = hasFeedback;
    }
}
