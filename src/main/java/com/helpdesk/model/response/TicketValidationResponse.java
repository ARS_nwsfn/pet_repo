package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;

import java.util.HashMap;
import java.util.Map;

public class TicketValidationResponse {

    @JsonView(Views.Public.class)
    private Map<String,String> fieldErrors = new HashMap<>();

    @JsonView(Views.Public.class)
    private String ticketId;

    public Map<String, String> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(Map<String, String> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }
}
