package com.helpdesk.model.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.dto.AttachmentDto;
import com.helpdesk.jsonview.Views;

import java.util.ArrayList;
import java.util.List;

public class AttachmentsResponse {

    @JsonView(Views.Public.class)
    List<AttachmentDto> attachments = new ArrayList<>();

    public List<AttachmentDto> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDto> files) {
        this.attachments = files;
    }
}
