package com.helpdesk.controllers.login;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("*")
public class LoginController {

    private static final Logger LOGGER = Logger.getLogger(LoginController.class);

    @GetMapping("/login")
    public ResponseEntity<String> loginPage(HttpServletRequest request, HttpServletResponse response) {

        LOGGER.info("login secured");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || auth.getName().equals("anonymousUser")) {
            return new ResponseEntity<>("not authorized", HttpStatus.FORBIDDEN);
        } else {
            return new ResponseEntity<>("already logged in", HttpStatus.OK);
        }
    }
}
