package com.helpdesk.controllers.tickets;

import com.helpdesk.entities.Attachment;
import com.helpdesk.model.response.AttachmentsResponse;
import com.helpdesk.model.response.SimpleMessageResponse;
import com.helpdesk.service.attachment.AttachmentService;
import com.helpdesk.service.attachment.AttachmentServiceForView;
import com.helpdesk.service.history.HistoryServiceForView;
import com.helpdesk.service.security.SecurityService;
import com.helpdesk.service.ticket.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/api/file")
public class AttachmentRestApiController {

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    AttachmentServiceForView attachmentServiceForView;

    @Autowired
    SecurityService securityService;

    @Autowired
    TicketService ticketService;

    @Autowired
    HistoryServiceForView historyServiceForView;

    @GetMapping("/getByTicket/{id}")
    public ResponseEntity<AttachmentsResponse> getAttachments(@PathVariable("id") Integer id) {

        AttachmentsResponse response = new AttachmentsResponse();
        if (id == null || id < 1) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        response.setAttachments(attachmentServiceForView.getAttachments(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}/download")
    public String downloadAttachment(@PathVariable(value = "id") int id, HttpServletResponse response) throws IOException {

        Attachment att = attachmentService.getAttachment(id);
        response.setContentType(att.getType());
        response.setContentLength(att.getContent().length);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + att.getName() + "\"");

        FileCopyUtils.copy(att.getContent(), response.getOutputStream());
        return null;
    }

    @DeleteMapping("/{fileId}/ticket/{ticketId}")
    public ResponseEntity<Long> deleteAttachment(@PathVariable("ticketId") int ticketId, @PathVariable("fileId") int fileId) {

        Attachment file = attachmentService.getAttachment(fileId);
        if (!securityService.getLoggedInUser().getId().equals(ticketService.getTicketById(ticketId).getOwnerId())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        attachmentService.deleteAttachment(fileId);
        historyServiceForView.addHistory(ticketId, "File deleted", " file " + file.getName() + " deleted");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/{ticketId}/upload")
    public ResponseEntity<SimpleMessageResponse> fileUpload(@PathVariable(value = "ticketId") int ticketId,
                                                            @RequestParam("file") MultipartFile file) {

        if (!securityService.getLoggedInUser().getId().equals(ticketService.getTicketById(ticketId).getOwnerId())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        attachmentServiceForView.uploadAttachment(file, ticketId);
        SimpleMessageResponse response = new SimpleMessageResponse();
        response.setMsg("File Uploaded Successfully.");
        historyServiceForView.addHistory(ticketId, "File uploaded", "new file " + file.getOriginalFilename() + " uploaded");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
