package com.helpdesk.controllers.tickets;


import com.helpdesk.dto.SpecificTicketDto;
import com.helpdesk.dto.TicketDto;
import com.helpdesk.entities.Category;
import com.helpdesk.entities.enums.State;
import com.helpdesk.entities.enums.Urgency;
import com.helpdesk.model.request.AjaxCommentRequestBody;
import com.helpdesk.model.request.AjaxFeedbackRequestbody;
import com.helpdesk.model.request.AjaxStateChangeRequestBody;
import com.helpdesk.model.response.*;
import com.helpdesk.service.category.CategoryService;
import com.helpdesk.service.comment.CommentServiceForView;
import com.helpdesk.service.email.EmailService;
import com.helpdesk.service.feedback.FeedbackServiceForView;
import com.helpdesk.service.history.HistoryServiceForView;
import com.helpdesk.service.ticket.TicketService;
import com.helpdesk.service.ticket.TicketServiceForView;
import com.helpdesk.validator.FileValidator;
import com.helpdesk.validator.TextValidator;
import com.helpdesk.validator.TicketForm;
import com.helpdesk.validator.TicketValidator;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/ticket")
public class TicketRestApiController {

    private static final Logger LOGGER = Logger.getLogger(TicketRestApiController.class);

    @Autowired
    TextValidator textValidator;

    @Autowired
    CommentServiceForView commentServiceForView;

    @Autowired
    FeedbackServiceForView feedbackServiceForView;

    @Autowired
    TicketService ticketService;

    @Autowired
    EmailService emailService;

    @Autowired
    TicketServiceForView ticketServiceForView;

    @Autowired
    CategoryService categoryService;

    @Autowired
    TicketValidator ticketValidator;

    @Autowired
    FileValidator fileValidator;

    @Autowired
    HistoryServiceForView historyServiceForView;

    @PostMapping(value = "/{ID}/add-comment")
    public SimpleMessageResponse createComment(@NotNull @RequestBody AjaxCommentRequestBody addCommentRequest, @PathVariable("ID") int id) {

        SimpleMessageResponse result = new SimpleMessageResponse();
        LOGGER.info("add comment " + addCommentRequest.getComment() + " to a ticket №" + id);
        if (textValidator.validateCommentText(addCommentRequest.getComment())) {
            commentServiceForView.saveComment(id, addCommentRequest.getComment());
            result.setCode("200");
            result.setMsg("comment saved");
            return result;
        } else {
            result.setCode("400");
            result.setMsg("comment not saved");
            return result;
        }

    }

    @PostMapping(value = "/{ID}/provide-feedback")
    public SimpleMessageResponse saveFeedback(@NotNull @RequestBody AjaxFeedbackRequestbody feedbackRequest, @PathVariable("ID") int id) {

        SimpleMessageResponse result = new SimpleMessageResponse();
        LOGGER.info(" ticket №" + id + " was rated: " + feedbackRequest.getRating());
        if (textValidator.validateCommentText(feedbackRequest.getComment())
                && ticketService.getTicketById(id).getState() == State.DONE) {
            feedbackServiceForView.saveFeedback(id, feedbackRequest.getComment(), feedbackRequest.getRating());
            emailService.sendEmailFeedbackProvided(id);
            result.setCode("200");
            result.setMsg("feedback saved");
            return result;
        } else {
            result.setCode("400");
            result.setMsg("feedback not saved");
            return result;
        }

    }

    @PutMapping(value = "/{ID}/change_state")
    public SimpleMessageResponse changeState(@NotNull @RequestBody AjaxStateChangeRequestBody stateRequest, @PathVariable("ID") int id) {

        SimpleMessageResponse result = new SimpleMessageResponse();
        LOGGER.info(" ticket №" + id + " change state to: " + stateRequest.getAction());
        if (ticketService.changeTicketState(id, stateRequest.getAction())) {
            result.setCode("200");
            result.setMsg("state changed");
            return result;
        } else {
            result.setCode("400");
            result.setMsg("state not changed");
            return result;
        }

    }

    @GetMapping(value = "/getAll")
    public TicketListResponse getFilteredAndOrderedTickets(@RequestParam(value = "orderBy", required = false) String orderBy,
                                                           @RequestParam(value = "orderType", required = false) String orderType,
                                                           @RequestParam(value = "filter", required = false) String filter,
                                                           @RequestParam(value = "task", required = false) String task) {

        LOGGER.info(" load Tickets orderBy: " + orderBy + " orderType: " + orderType + " filter: " + filter + " task: " + task);
        TicketListResponse result = new TicketListResponse();
        List<TicketDto> tickets = ticketServiceForView.getTickets(task);
        tickets = ticketServiceForView.getFilteredAndOrderedTickets(tickets, orderBy, orderType, filter);
        tickets = ticketServiceForView.getActionsForTicket(tickets);
        result.setCode("200");
        result.setTickets(tickets);
        return result;
    }

    @GetMapping(value = "/getCategories")
    public StringListOfOptionsResponse getAllCategories() {

        StringListOfOptionsResponse result = new StringListOfOptionsResponse();
        result.setCode("200");
        result.setOptions(categoryService.getAllCategories().stream().map(Category::getName).collect(Collectors.toList()));
        return result;
    }

    @GetMapping(value = "/getUrgencies")
    public StringListOfOptionsResponse getAllUrgencies() {

        StringListOfOptionsResponse result = new StringListOfOptionsResponse();
        result.setCode("200");
        result.setOptions(Stream.of(Urgency.values())
                .map(Urgency::name)
                .collect(Collectors.toList()));
        return result;
    }

    @PostMapping("/create")
    public ResponseEntity<TicketValidationResponse> createNewTicket(@ModelAttribute @Valid TicketForm ticketForm,
                                                                    BindingResult result, Model model,
                                                                    @RequestParam(required = false, value = "status") String status) {

        ticketValidator.validate(ticketForm, result);
        fileValidator.validate(ticketForm.getFile(), result);
        TicketValidationResponse resp = new TicketValidationResponse();
        resp.setFieldErrors(ticketServiceForView.convertBindingResult(result));
        if (result.hasErrors()) {
            return new ResponseEntity<>(resp,HttpStatus.BAD_REQUEST);
        }
        if (status.equals("submit")) {
            ticketForm.setState(State.NEW);
        } else {
            ticketForm.setState(State.DRAFT);
        }
        ticketServiceForView.saveTicket(ticketForm);
        emailService.sendEmailTicketForApproval(ticketForm.getId());
        resp.setTicketId(ticketForm.getId().toString());
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @GetMapping("/{id}/edit")
    public ResponseEntity<TicketForm> getTicket(@PathVariable("id") Integer id) {

        TicketForm response = new TicketForm();
        if (id == null || id < 1) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        response = ticketServiceForView.getTicketForEdit(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetSpecificTicketResponse> getSpecificTicket(@PathVariable("id") Integer id) {

        GetSpecificTicketResponse response = new GetSpecificTicketResponse();
        if (id == null || id < 1) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        SpecificTicketDto ticketDto = ticketServiceForView.getSpecificTicket(id);
        if (ticketDto == null) return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        response.setTicket(ticketDto);
        response.setOwner(ticketService.checkIfUserIsOwner(id));
        response.setFeedback(feedbackServiceForView.checkForFeedback(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/{id}/edit")
    public ResponseEntity<TicketValidationResponse> editTicket(@ModelAttribute @Valid TicketForm ticketForm,
                                                                    BindingResult result, Model model,
                                                                    @RequestParam(required = false, value = "status") String status,
                                                                    @PathVariable("id") int id) {

        TicketValidationResponse resp = new TicketValidationResponse();
        if (!ticketService.checkIfUserIsOwner(id)){
            return new ResponseEntity<>(resp,HttpStatus.FORBIDDEN);
        }
        ticketValidator.validate(ticketForm, result);
        fileValidator.validate(ticketForm.getFile(), result);
        resp.setFieldErrors(ticketServiceForView.convertBindingResult(result));
        if (result.hasErrors()) {
            return new ResponseEntity<>(resp,HttpStatus.BAD_REQUEST);
        }
        if (status.equals("submit")) {
            ticketForm.setState(State.NEW);
        } else {
            ticketForm.setState(State.DRAFT);
        }
        ticketServiceForView.editTicket(ticketForm);
        emailService.sendEmailTicketForApproval(ticketForm.getId());
        resp.setTicketId(Integer.toString(id));
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @GetMapping(path = {"/{id}/history"})
    public ResponseEntity<TicketHistoryResponse> loadHistory(@PathVariable(value = "id") int id, @RequestParam(required = false, value = "amount") int amount) {

        TicketHistoryResponse response = new TicketHistoryResponse();
        response.setHistories(historyServiceForView.getHistoryByTicketId(id, amount));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = {"/{id}/comments"})
    public ResponseEntity<TicketCommentsResponse> loadComments(@PathVariable(value = "id") int id, @RequestParam(required = false, value = "amount") int amount) {

        TicketCommentsResponse response = new TicketCommentsResponse();
        response.setComments(commentServiceForView.getCommentsByTicketId(id, amount));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = {"/{id}/feedback"})
    public ResponseEntity<FeedbackResponse> showFeedback(@PathVariable(value = "id") Integer id) {

        LOGGER.info("showing feedback for ticket № " + id );
        if( id == null || id < 1){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        SpecificTicketDto ticket = ticketServiceForView.getSpecificTicket(id);
        if( ticket.getState() != State.DONE){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        FeedbackResponse response = new FeedbackResponse();
        response.setHasFeedback(false);
        response.setTicketName(ticket.getName());
        if (feedbackServiceForView.getFeedbackDtoByTicketId(id) != null){
            response.setFeedback(feedbackServiceForView.getFeedbackDtoByTicketId(id));
            response.setHasFeedback(true);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
