package com.helpdesk.controllers.user;

import com.fasterxml.jackson.annotation.JsonView;
import com.helpdesk.jsonview.Views;
import com.helpdesk.model.response.AuthResponse;
import com.helpdesk.service.security.SecurityService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserApiController {

    private static final Logger LOGGER = Logger.getLogger(UserApiController.class);

    @Autowired
    SecurityService securityService;


    @JsonView(Views.Public.class)
    @GetMapping(value = "/getAuth")
    public ResponseEntity<AuthResponse> getAuth() {

        AuthResponse result = new AuthResponse();
        LOGGER.info(" current user:" + securityService.getLoggedInUser().getEmail());
        result.setCode("200");
        if (!securityService.getLoggedInUser().getEmail().equals("anonymousUser")) {
            result.setEmail(securityService.getLoggedInUser().getEmail());
            result.setRole(securityService.getLoggedInUser().getRole().name());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
