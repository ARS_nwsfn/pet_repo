package com.helpdesk.service.user;

import com.helpdesk.dao.UserDao;
import com.helpdesk.dto.UserDto;
import com.helpdesk.entities.User;
import com.helpdesk.service.security.SecurityService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    SecurityService securityService;

    @Transactional
    public void saveUser(User user) {
        userDao.save(user);
    }

    @Transactional
    public User getUserById(int id) {
        return userDao.getById(id);
    }

    @Transactional
    public void updateUser(User user) {
        userDao.update(user);
    }

    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userDao.getAll();
    }

    @Transactional(readOnly = true)
    public User findByUserName(String name) {
        return userDao.findByEmail(name);
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String email){

        User user = userDao.findByEmail(email);
        LOGGER.info("this user tried to login >> {}", email);
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;

        if (user != null) {
            builder = org.springframework.security.core.userdetails.User.withUsername(email);
            builder.password(user.getPassword());
            String[] authorities = {user.getRole().toString()};
            builder.authorities(authorities);
        } else {
            throw new UsernameNotFoundException("User not found.");
        }
        return builder.build();
    }

    public UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

}
