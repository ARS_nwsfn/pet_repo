package com.helpdesk.service.user;

import com.helpdesk.dto.UserDto;
import com.helpdesk.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void saveUser(User user);

    void updateUser(User user);

    List<User> getAllUsers();

    User findByUserName(String name);

    User getUserById(int id);

    UserDto convertToDto(User user);
}
