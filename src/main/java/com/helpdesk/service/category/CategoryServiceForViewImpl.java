package com.helpdesk.service.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceForViewImpl implements CategoryServiceForView {

    @Autowired
    private CategoryService categoryService;

    @Override
    public List<String> getAllCategories() {
        List<String> categories = new ArrayList();
        categoryService.getAllCategories().forEach(category -> categories.add(category.getName()));
        return categories;
    }
}
