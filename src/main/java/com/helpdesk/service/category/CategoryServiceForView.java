package com.helpdesk.service.category;

import java.util.List;

public interface CategoryServiceForView {

    List<String> getAllCategories();
}
