package com.helpdesk.service.category;

import com.helpdesk.entities.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategories();
}
