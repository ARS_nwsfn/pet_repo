package com.helpdesk.service.ticket;

import com.helpdesk.dto.SpecificTicketDto;
import com.helpdesk.dto.TicketDto;
import com.helpdesk.entities.Attachment;
import com.helpdesk.entities.Ticket;
import com.helpdesk.entities.User;
import com.helpdesk.entities.enums.State;
import com.helpdesk.service.attachment.AttachmentService;
import com.helpdesk.service.history.HistoryService;
import com.helpdesk.service.security.SecurityService;
import com.helpdesk.service.user.UserService;
import com.helpdesk.validator.TicketForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TicketServiceForViewImpl implements TicketServiceForView {

    private static final Set<String> STATES_FOR_MANAGER = Stream.of(State.APPROVED.name(), State.DECLINED.name(),
            State.CANCELED.name(), State.INPROGRESS.name(), State.DONE.name())
            .collect(Collectors.toSet());

    private static final Set<String> STATES_FOR_ENGINEER = Stream.of(State.INPROGRESS.name(), State.DONE.name())
            .collect(Collectors.toSet());

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private MessageSource messageSource;


    @Override
    public List<TicketDto> getAllTickets() {
        List<Ticket> tickets = ticketService.getAllTickets();
        return tickets.stream()
                .map(ticket -> ticketService.convertTicketToDto(ticket))
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getTicketsByUserId(int id) {
        List<Ticket> tickets = ticketService.getTicketsByUserId(id);
        return tickets.stream()
                .map(ticket -> ticketService.convertTicketToDto(ticket))
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getTicketsByState(State state) {
        List<Ticket> tickets = ticketService.getTicketsByState(state);
        return tickets.stream()
                .map(ticket -> ticketService.convertTicketToDto(ticket))
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getTicketsByApproverId(int id) {
        List<Ticket> tickets = ticketService.getTicketsByApproverId(id);
        return tickets.stream()
                .map(ticket -> ticketService.convertTicketToDto(ticket))
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getTicketsByAssigneeId(int id) {
        List<Ticket> tickets = ticketService.getTicketsByAssigneeId(id);
        return tickets.stream()
                .map(ticket -> ticketService.convertTicketToDto(ticket))
                .collect(Collectors.toList());
    }

    @Override
    public List<TicketDto> getTickets(String byTask) {
        if (byTask == null || byTask.equals("all")) {
            return getAllTickets();
        }
        if (byTask.equals("my")) {
            return getTicketsByUserId(securityService.getLoggedInUser().getId());
        }
        if (byTask.equals("new")) {
            return getTicketsByState(State.NEW);
        }
        if (byTask.equals("manager")) {
            return getTicketsByApproverId(securityService.getLoggedInUser().getId()).stream()
                    .filter(ticket -> STATES_FOR_MANAGER.contains(ticket.getState().name()))
                    .collect(Collectors.toList());
        }
        if (byTask.equals("approved")) {
            return getTicketsByState(State.APPROVED);
        }
        if (byTask.equals("assign")) {
            return getTicketsByAssigneeId(securityService.getLoggedInUser().getId()).stream()
                    .filter(ticket -> STATES_FOR_ENGINEER.contains(ticket.getState().name()))
                    .collect(Collectors.toList());
        }
        return getAllTickets();
    }

    public List<TicketDto> filterTickets(List<TicketDto> tickets, String filterBy) {
        Predicate<TicketDto> isIdContains = ticket -> ticket.getId().toString().contains(filterBy);
        Predicate<TicketDto> isNameContains = ticket -> ticket.getName().contains(filterBy);
        Predicate<TicketDto> isDateContains = ticket -> ticket.getDesiredResolutionDate().getTime().toString().contains(filterBy);
        Predicate<TicketDto> isUrgencyContains = ticket -> ticket.getUrgency().toString().toLowerCase().contains(filterBy);
        Predicate<TicketDto> isStateContains = ticket -> ticket.getState().toString().toLowerCase().contains(filterBy);
        return tickets.stream()
                .filter(isIdContains.or(isNameContains).or(isDateContains).or(isUrgencyContains).or(isStateContains))
                .collect(Collectors.toList());
    }

    public List<TicketDto> orderTickets(List<TicketDto> tickets, String orderBy) {

        if (orderBy.equals("id")) {
            tickets.sort(Comparator.comparing(TicketDto::getId));
        }
        if (orderBy.equals("name")) {
            tickets.sort(Comparator.comparing(TicketDto::getName));
        }
        if (orderBy.equals("date")) {
            tickets.sort(Comparator.comparing(TicketDto::getDesiredResolutionDate));
        }
        if (orderBy.equals("urgency")) {
            tickets.sort(Comparator.comparing(TicketDto::getUrgency));
        }
        if (orderBy.equals("state")) {
            tickets.sort(Comparator.comparing((TicketDto ticket) -> ticket.getState().name()));
        }
        return tickets;
    }

    public List<TicketDto> getFilteredAndOrderedTickets(List<TicketDto> tickets, String orderBy, String orderType, String filterBy) {
        if (filterBy != null) {
            tickets = filterTickets(tickets, filterBy);
        }
        if (orderBy != null && orderType != null) {
            orderTickets(tickets, orderBy);
            if (orderType.equals("desc")) {
                Collections.reverse(tickets);
            }
        }
        return tickets;
    }

    @Override
    public void saveTicket(TicketForm ticket) {
        ticketService.saveTicket(ticket);
    }

    @Override
    public void editTicket(TicketForm ticket) {
        ticketService.updateTicket(ticket);
    }

    @Override
    public SpecificTicketDto getSpecificTicket(int id) {

        Ticket ticket = ticketService.getTicketById(id);
        SpecificTicketDto ticketForView = new SpecificTicketDto();
        ticketForView.setId(id);
        ticketForView.setName(ticket.getName());
        ticketForView.setCreatedOn(ticket.getCreatedOn());
        ticketForView.setState(ticket.getState());
        ticketForView.setUrgency(ticket.getUrgency());
        ticketForView.setDesiredResolutionDate(ticket.getDesiredResolutionDate());
        User user = userService.getUserById(ticket.getOwnerId());
        ticketForView.setOwner(user.getFirstName() + " " + user.getLastName());
        if (ticket.getApproverId() != null) {
            user = userService.getUserById(ticket.getApproverId());
            ticketForView.setApprover(user.getFirstName() + " " + user.getLastName());
        }
        if (ticket.getAssigneeId() != null) {
            user = userService.getUserById(ticket.getAssigneeId());
            ticketForView.setAssignee(user.getFirstName() + " " + user.getLastName());
        }
        ticketForView.setDescription(ticket.getDescription());
        ticketForView.setCategory(ticket.getCategory());
        return ticketForView;
    }

    @Override
    public void deleteAttachmentFromTicket(int ticketId) {
        List<Attachment> attachments = attachmentService.getAttachmentByTicketId(ticketId);
        if (!attachments.isEmpty()){
            historyService.addHistory(ticketId,"attachment deleted", "File: " + attachments.get(0).getName() + " deleted" );
            attachmentService.deleteAttachment(attachments.get(0));
        }
    }

    @Override
    public TicketForm getTicketForEdit(int ticketId) {

        TicketForm ticketForEdit = new TicketForm();
        Ticket ticket = ticketService.getTicketById(ticketId);
        ticketForEdit.setId(ticket.getId());
        ticketForEdit.setState(ticket.getState());
        ticketForEdit.setCategory(ticket.getCategory());
        ticketForEdit.setDescription(ticket.getDescription());
        ticketForEdit.setName(ticket.getName());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (ticket.getDesiredResolutionDate() != null) {
            ticketForEdit.setDesiredResolutionDate(sdf.format(ticket.getDesiredResolutionDate().getTime()));
        }
        ticketForEdit.setUrgency(ticket.getUrgency().name());
        return ticketForEdit;
    }

    @Override
    public List<TicketDto> getActionsForTicket(List<TicketDto> tickets) {

        tickets.forEach(ticket -> ticket.setActions(ticketService.getActionsForTicket(ticket.getId())));
        return tickets;
    }

    @Override
    public Map<String,String> convertBindingResult(BindingResult bindingResult){
        Map<String,String> fieldErrors = new HashMap<>();
        int i = 0;
        for(ObjectError object : bindingResult.getAllErrors()){
            if(object instanceof FieldError) {
                FieldError fieldError = (FieldError) object;
                if (fieldError.getCode() != null) {
                    fieldErrors.put(fieldError.getField() + i, messageSource.getMessage(fieldError.getCode(), null, Locale.US));
                }
            }
            i++;
        }
        return fieldErrors;
    }

}
