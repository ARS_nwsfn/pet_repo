package com.helpdesk.service.ticket;

import com.helpdesk.dao.TicketDao;
import com.helpdesk.dto.TicketDto;
import com.helpdesk.dto.UserDto;
import com.helpdesk.entities.Ticket;
import com.helpdesk.entities.User;
import com.helpdesk.entities.enums.Role;
import com.helpdesk.entities.enums.State;
import com.helpdesk.entities.enums.Urgency;
import com.helpdesk.service.attachment.AttachmentService;
import com.helpdesk.service.comment.CommentService;
import com.helpdesk.service.email.EmailService;
import com.helpdesk.service.history.HistoryService;
import com.helpdesk.service.security.SecurityService;
import com.helpdesk.service.user.UserService;
import com.helpdesk.validator.TicketForm;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    private static final Logger LOGGER = Logger.getLogger(TicketServiceImpl.class);

    private static final String SUBMIT_ACTION = "SUBMIT";

    private static final String CANCEL_ACTION = "CANCEL";

    private static final String APPROVE_ACTION = "APPROVE";

    private static final String DECLINE_ACTION = "DECLINE";

    private static final String ASSIGN_TO_ME_ACTION = "ASSIGN TO ME";

    private static final String DONE_ACTION = "DONE";

    @Autowired
    private TicketDao ticketDao;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Override
    public void saveTicket(Ticket ticket) {
        ticketDao.save(ticket);
    }

    @Override
    public void updateTicket(Ticket ticket) {
        ticketDao.update(ticket);
    }

    @Override
    public void saveTicket(TicketForm ticketForm) {

        int userID = securityService.getLoggedInUser().getId();
        Ticket ticket = new Ticket();
        setTicketFields(ticket,ticketForm);
        saveTicket(ticket);
        ticketForm.setId(ticket.getId());
        if (!ticketForm.getComment().isEmpty()) {
            commentService.saveComment(ticketForm.getComment(),ticket.getId(),userID);
        }
        if (!ticketForm.getFile().isEmpty()) {
            attachmentService.saveAttachment(ticketForm.getFile(), ticket.getId());
        }
        historyService.addHistory(ticket.getId(), "ticket created", "ticket created, name: " + ticket.getName());
    }

    @Override
    public void updateTicket(TicketForm ticketForm) {
        int userID = securityService.getLoggedInUser().getId();
        Ticket ticket = getTicketById(ticketForm.getId());
        setTicketFields(ticket,ticketForm);
        updateTicket(ticket);
        ticketForm.setId(ticket.getId());
        if (!ticketForm.getComment().isEmpty()) {
            commentService.saveComment(ticketForm.getComment(),ticket.getId(),userID);
        }
        if (!ticketForm.getFile().isEmpty()) {
            attachmentService.saveAttachment(ticketForm.getFile(), ticket.getId());
        }
        historyService.addHistory(ticket.getId(),
                "ticket edited",
                "ticket edited by: " +
                        securityService.getLoggedInUser().getFirstName() +
                        " " + securityService.getLoggedInUser().getLastName());
    }

    private void setTicketFields(Ticket ticket, TicketForm ticketForm){
        int userID = securityService.getLoggedInUser().getId();
        ticket.setOwnerId(userID);
        if (!ticketForm.getName().isEmpty()) {
            ticket.setName(ticketForm.getName());
        }
        ticket.setState(ticketForm.getState());
        ticket.setUrgency(Urgency.valueOf(ticketForm.getUrgency()));
        ticket.setDescription(ticketForm.getDescription());
        ticket.setCategory(ticketForm.getCategory());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(ticketForm.getDesiredResolutionDate()));
        } catch (Exception e) {
            LOGGER.error("couldn't convert to date " + e.getMessage());
        }
        ticket.setDesiredResolutionDate(calendar);
    }


    @Override
    public void changeState(int ticketId, State state) {
        Ticket ticket = getTicketById(ticketId);
        ticket.setState(state);
        updateTicket(ticket);
    }

    @Override
    public void setApprover(int ticketId) {
        Ticket ticket = getTicketById(ticketId);
        ticket.setApproverId(securityService.getLoggedInUser().getId());
        updateTicket(ticket);
    }

    @Override
    public void setAssignee(int ticketId) {
        Ticket ticket = getTicketById(ticketId);
        ticket.setAssigneeId(securityService.getLoggedInUser().getId());
        updateTicket(ticket);
    }

    @Override
    public Ticket getTicketById(int id) {
        return ticketDao.getById(id);
    }

    @Override
    public List<Ticket> getAllTickets() {
        return ticketDao.getAll();
    }

    @Override
    public List<Ticket> getTicketsByUserId(int id) {
        return ticketDao.getByUserId(id);
    }

    @Override
    public List<Ticket> getTicketsByState(State state) {
        return ticketDao.getByState(state);
    }

    @Override
    public List<Ticket> getTicketsByApproverId(int id) {
        return ticketDao.getByApproverId(id);
    }

    @Override
    public List<Ticket> getTicketsByAssigneeId(int id) {
        return ticketDao.getByAssigneeId(id);
    }

    @Override
    public TicketDto convertTicketToDto(Ticket ticket) {
        return modelMapper.map(ticket, TicketDto.class);
    }

    @Override
    public Ticket convertTicketDtoToEntity(TicketDto ticketDto) {
        return modelMapper.map(ticketDto, Ticket.class);
    }

    @Override
    public boolean checkIfUserIsOwner(int ticketId) {

        LOGGER.info("ticket owner id: " + getTicketById(ticketId).getOwnerId());
        LOGGER.info("ticket logged in user id: " + securityService.getLoggedInUser().getId());
        return getTicketById(ticketId).getOwnerId().equals(securityService.getLoggedInUser().getId());
    }

    @Override
    public List<String> getActionsForTicket(int ticketId) {
        Ticket ticket = getTicketById(ticketId);
        List<String> actions = new ArrayList<>();
        UserDto loggedUser = securityService.getLoggedInUser();
        if (ticket.getState() == State.DRAFT && loggedUser.getId().equals(ticket.getOwnerId())) {
            actions.add(SUBMIT_ACTION);
            actions.add(CANCEL_ACTION);
        }
        if (ticket.getState() == State.NEW && loggedUser.getRole() == Role.MANAGER && !loggedUser.getId().equals(ticket.getOwnerId())) {
            actions.add(APPROVE_ACTION);
            actions.add(DECLINE_ACTION);
            actions.add(CANCEL_ACTION);
        }
        if (ticket.getState() == State.APPROVED && loggedUser.getRole() == Role.ENGINEER) {
            actions.add(ASSIGN_TO_ME_ACTION);
            actions.add(CANCEL_ACTION);
        }
        if (ticket.getState() == State.DECLINED && loggedUser.getId().equals(ticket.getOwnerId())) {
            actions.add(SUBMIT_ACTION);
            actions.add(CANCEL_ACTION);
        }
        if (ticket.getState() == State.INPROGRESS && loggedUser.getRole() == Role.ENGINEER && ticket.getAssigneeId().equals(loggedUser.getId())) {
            actions.add(DONE_ACTION);
        }
        return actions;
    }

    @Override
    public boolean changeTicketState(int ticketId, String action) {

        boolean stateChanged;

        switch (action) {
            case SUBMIT_ACTION:
                stateChanged = submitAction(ticketId);
                emailService.sendEmailTicketForApproval(ticketId);
                break;
            case APPROVE_ACTION:
                stateChanged = approveAction(ticketId);
                emailService.sendEmailTicketApproved(ticketId);
                break;
            case ASSIGN_TO_ME_ACTION:
                stateChanged = assignAction(ticketId);
                break;
            case DECLINE_ACTION:
                stateChanged = declineAction(ticketId);
                emailService.sendEmailTicketDeclined(ticketId);
                break;
            case CANCEL_ACTION:
                stateChanged = cancelAction(ticketId);
                if (getTicketById(ticketId).getState() == State.NEW){
                    emailService.sendEmailTicketCancelledByManager(ticketId);
                }
                if (getTicketById(ticketId).getState() == State.APPROVED){
                    emailService.sendEmailTicketCancelledByEngineer(ticketId);
                }
                break;
            case DONE_ACTION:
                stateChanged = doneAction(ticketId);
                emailService.sendEmailTicketDone(ticketId);
                break;
            default:
                stateChanged = false;
        }
        return stateChanged;
    }

    private boolean submitAction(int id) {

        LOGGER.info("submit ticket №" + id);
        if (!checkIfUserIsOwner(id) && (getTicketById(id).getState() != State.DRAFT || getTicketById(id).getState() != State.DECLINED)) {
            return false;
        }
        changeState(id, State.NEW);
        return true;
    }

    private boolean approveAction(int id) {

        LOGGER.info("approve ticket №" + id);
        if (getTicketById(id).getState() != State.NEW || securityService.getLoggedInUser().getRole() != Role.MANAGER) {
            return false;
        }
        changeState(id, State.APPROVED);
        setApprover(id);
        return true;
    }

    private boolean assignAction(int id) {

        LOGGER.info("assign ticket №" + id);
        if (getTicketById(id).getState() != State.APPROVED || securityService.getLoggedInUser().getRole() != Role.ENGINEER) {
            return false;
        }
        changeState(id, State.INPROGRESS);
        setAssignee(id);
        return true;
    }

    private boolean declineAction(int id) {

        LOGGER.info("decline ticket №" + id);
        if (getTicketById(id).getState() != State.NEW || securityService.getLoggedInUser().getRole() != Role.MANAGER) {
            return false;
        }
        changeState(id, State.DECLINED);
        return true;
    }

    private boolean cancelAction(int id) {

        LOGGER.info("cancel ticket №" + id);
        UserDto loggedUser = securityService.getLoggedInUser();
        if (getTicketById(id).getState() != State.NEW &&
                getTicketById(id).getState() != State.DECLINED &&
                getTicketById(id).getState() != State.APPROVED &&
                getTicketById(id).getState() != State.DRAFT) {
            LOGGER.info("ticket in a wrong state for cancel, current state: " + getTicketById(id).getState());
            return false;
        }

        if (getTicketById(id).getState() == State.DRAFT && !checkIfUserIsOwner(id)) {
            LOGGER.info("you are not an owner to cancel from draft");
            return false;
        }

        if (getTicketById(id).getState() == State.NEW && loggedUser.getRole() != Role.MANAGER) {
            LOGGER.info("you are not a manager to cancel from new");
            return false;
        }

        if (getTicketById(id).getState() == State.APPROVED && loggedUser.getRole() != Role.ENGINEER) {
            LOGGER.info("you are not an engineer to cancel from approved");
            return false;
        }

        if (getTicketById(id).getState() == State.DECLINED && !checkIfUserIsOwner(id)) {
            LOGGER.info("you are not an owner to cancel from declined");
            return false;
        }


        changeState(id, State.CANCELED);
        return true;
    }

    private boolean doneAction(int id) {

        LOGGER.info("decline ticket №" + id);
        if (getTicketById(id).getState() != State.INPROGRESS || securityService.getLoggedInUser().getRole() != Role.ENGINEER) {
            return false;
        }
        changeState(id, State.DONE);
        return true;
    }

    @Override
    public User getTicketOwner(int ticketId) {
        return userService.getUserById(getTicketById(ticketId).getOwnerId());
    }

    @Override
    public User getTicketManager(int ticketId) {
        return userService.getUserById(getTicketById(ticketId).getApproverId());
    }

    @Override
    public User getTicketEngineer(int ticketId) {
        return userService.getUserById(getTicketById(ticketId).getAssigneeId());
    }
}