package com.helpdesk.service.ticket;

import com.helpdesk.dto.SpecificTicketDto;
import com.helpdesk.dto.TicketDto;
import com.helpdesk.entities.enums.State;
import com.helpdesk.validator.TicketForm;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Map;


public interface TicketServiceForView {

    List<TicketDto> getAllTickets();
    List<TicketDto> getTicketsByUserId(int id);
    List<TicketDto> getTicketsByState(State state);
    List<TicketDto> getTicketsByApproverId(int id);
    List<TicketDto> getTicketsByAssigneeId(int id);

    List<TicketDto> filterTickets(List<TicketDto> tickets, String filterBy);
    List<TicketDto> orderTickets(List<TicketDto> tickets, String orderBy);
    List<TicketDto> getFilteredAndOrderedTickets(List<TicketDto> tickets, String orderBy, String orderType, String filterBy);
    List<TicketDto> getTickets(String byOwner);
    List<TicketDto> getActionsForTicket( List<TicketDto> tickets);

    SpecificTicketDto getSpecificTicket(int id);

    void saveTicket(TicketForm ticket);
    void editTicket(TicketForm ticket);
    void deleteAttachmentFromTicket(int ticketId);

    TicketForm getTicketForEdit(int ticketid);
    Map<String,String> convertBindingResult(BindingResult bindingResult);

}
