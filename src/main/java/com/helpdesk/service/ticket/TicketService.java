package com.helpdesk.service.ticket;

import com.helpdesk.dto.TicketDto;
import com.helpdesk.entities.Ticket;
import com.helpdesk.entities.User;
import com.helpdesk.entities.enums.State;
import com.helpdesk.validator.TicketForm;

import java.util.List;

public interface TicketService {

    void saveTicket(Ticket ticket);

    void saveTicket(TicketForm ticket);

    void updateTicket(Ticket ticket);

    void updateTicket(TicketForm ticket);

    void changeState(int ticketId, State state);

    boolean changeTicketState(int ticketId, String action);

    void setApprover(int ticketId);

    void setAssignee(int ticketId);



    User getTicketOwner(int ticketId);

    User getTicketManager(int ticketId);

    User getTicketEngineer(int ticketId);

    Ticket getTicketById(int id);

    List<Ticket> getAllTickets();

    List<Ticket> getTicketsByUserId(int id);

    List<Ticket> getTicketsByState(State state);

    List<Ticket> getTicketsByApproverId(int id);

    List<Ticket> getTicketsByAssigneeId(int id);

    TicketDto convertTicketToDto(Ticket ticket);

    Ticket convertTicketDtoToEntity(TicketDto ticketDto);
    
    boolean checkIfUserIsOwner(int ticketId);

    List<String> getActionsForTicket(int ticketId);

}
