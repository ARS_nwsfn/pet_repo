package com.helpdesk.service.email;

public interface EmailService {

    void sendSimpleMessage(String to, String subject, String text);

    void sendEmailTicketForApproval(int ticketId);

    void sendEmailTicketApproved(int ticketId);

    void sendEmailTicketDeclined(int ticketId);

    void sendEmailTicketCancelledByManager(int ticketId);

    void sendEmailTicketCancelledByEngineer(int ticketId);

    void sendEmailTicketDone(int ticketId);

    void sendEmailFeedbackProvided(int ticketId);
}
