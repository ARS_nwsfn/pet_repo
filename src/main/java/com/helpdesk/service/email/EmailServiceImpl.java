package com.helpdesk.service.email;

import com.helpdesk.service.ticket.TicketService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;

@Component
public class EmailServiceImpl implements EmailService {

    private static final Logger LOGGER = Logger.getLogger(EmailServiceImpl.class);

    @Autowired
    TicketService ticketService;
    @Autowired
    private JavaMailSender emailSender;
    @Autowired
    private SpringTemplateEngine templateEngine;

    @Value("${mail.mailsender.sendto}")
    private String sendToEmail;

    @Value("${mail.mailsender.ticket.approval}")
    private String newTicketApproval;

    @Value("${mail.mailsender.ticket.approved}")
    private String ticketApproved;

    @Value("${mail.mailsender.ticket.declined}")
    private String ticketDeclined;

    @Value("${mail.mailsender.ticket.cancelled}")
    private String ticketCancelled;

    @Value("${mail.mailsender.ticket.done}")
    private String ticketDone;

    @Value("${mail.mailsender.ticket.feedbackprovided}")
    private String feedbackProvided;

    public void sendSimpleMessage(
            String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    @Override
    public void sendEmailTicketForApproval(int ticketId) {

        sendEmail(ticketId, "emails/NewTicketForApproval", newTicketApproval);
    }

    @Override
    public void sendEmailTicketApproved(int ticketId) {

        sendEmail(ticketId, "emails/TicketWasApproved", ticketApproved);
    }

    @Override
    public void sendEmailTicketDeclined(int ticketId) {

        sendEmail(ticketId, "emails/TicketDeclined", ticketDeclined);
    }

    @Override
    public void sendEmailTicketCancelledByManager(int ticketId) {

        sendEmail(ticketId, "emails/TicketCancelledByManager", ticketCancelled);
    }

    @Override
    public void sendEmailTicketCancelledByEngineer(int ticketId) {

        sendEmail(ticketId, "emails/TicketCancelledByEngineer", ticketCancelled);
    }

    @Override
    public void sendEmailTicketDone(int ticketId) {

        sendEmail(ticketId, "emails/TicketWasDone", ticketDone);
    }

    @Override
    public void sendEmailFeedbackProvided(int ticketId) {

        sendEmail(ticketId, "emails/FeedbackProvided", feedbackProvided);
    }

    private void sendEmail(int ticketId, String template, String subject) {
        MimeMessage message = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Context context = new Context();
            context.setVariable("subject", subject);
            context.setVariable("ticketId", ticketId);
            String html = templateEngine.process(template, context);

            helper.setTo(sendToEmail);
            helper.setText(html, true);
            helper.setSubject(newTicketApproval);
            helper.setFrom(ticketService.getTicketOwner(ticketId).getFirstName() + " " + ticketService.getTicketOwner(ticketId).getLastName());

            emailSender.send(message);
        } catch (MessagingException e) {
            LOGGER.warn("cannot send email" + subject + " about ticket with id: " + ticketId);
        }
    }
}