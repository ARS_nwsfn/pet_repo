package com.helpdesk.service.feedback;

import com.helpdesk.dto.FeedbackDto;
import com.helpdesk.entities.Feedback;

import java.util.List;

public interface FeedbackServiceForView {

    List<Feedback> getFeedbackByTicketId(int id);

    FeedbackDto getFeedbackDtoByTicketId(int id);

    void saveFeedback(int ticketId, String comment, int rating);

    boolean checkForFeedback(int ticketId);
}
