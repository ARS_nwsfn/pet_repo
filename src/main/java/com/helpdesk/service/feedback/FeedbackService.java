package com.helpdesk.service.feedback;

import com.helpdesk.dto.FeedbackDto;
import com.helpdesk.entities.Feedback;

import java.util.List;

public interface FeedbackService {

    void saveFeedback(Feedback feedback);

    List<Feedback> getFeedbackByTicketId(int id);

    FeedbackDto convertFeedbackToDto(Feedback ticket);
}
