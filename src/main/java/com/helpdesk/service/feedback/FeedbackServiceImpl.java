package com.helpdesk.service.feedback;

import com.helpdesk.dao.FeedbackDao;
import com.helpdesk.dto.FeedbackDto;
import com.helpdesk.entities.Feedback;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackDao feedbackDao;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void saveFeedback(Feedback feedback) {
        feedbackDao.save(feedback);
    }

    @Override
    public List<Feedback> getFeedbackByTicketId(int id) {
        return feedbackDao.getByTicketId(id);
    }

    @Override
    public FeedbackDto convertFeedbackToDto(Feedback feedback) {
        return modelMapper.map(feedback, FeedbackDto.class);
    }

}
