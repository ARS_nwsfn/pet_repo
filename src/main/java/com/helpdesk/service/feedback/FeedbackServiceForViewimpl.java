package com.helpdesk.service.feedback;

import com.helpdesk.dto.FeedbackDto;
import com.helpdesk.entities.Feedback;
import com.helpdesk.service.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class FeedbackServiceForViewimpl implements FeedbackServiceForView {

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private SecurityService securityService;

    @Override
    public List<Feedback> getFeedbackByTicketId(int id) {

        return feedbackService.getFeedbackByTicketId(id);
    }

    @Override
    public FeedbackDto getFeedbackDtoByTicketId(int id) {

        List<Feedback> feed = feedbackService.getFeedbackByTicketId(id);
        if (!feed.isEmpty()){
            return feedbackService.convertFeedbackToDto(feed.get(0));
        }
        return null;
    }

    @Override
    public void saveFeedback(int ticketId, String comment, int rating) {
        Feedback feedback = new Feedback();
        feedback.setDate(Calendar.getInstance());
        feedback.setRate(rating);
        feedback.setTicketId(ticketId);
        feedback.setText(comment);
        feedback.setUserId(securityService.getLoggedInUser().getId());
        feedbackService.saveFeedback(feedback);
    }

    @Override
    public boolean checkForFeedback(int ticketId) {

        return !getFeedbackByTicketId(ticketId).isEmpty();
    }
}
