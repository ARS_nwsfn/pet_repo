package com.helpdesk.service.security;

import com.helpdesk.dto.UserDto;
import com.helpdesk.entities.User;
import com.helpdesk.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImp implements SecurityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityServiceImp.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Override
    public void autoLogin(String email, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        authenticationManager.authenticate(authenticationToken);

        if (authenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);

            LOGGER.info("Successfully auto logged in {}", email);
        }
    }

    @Override
    public UserDto getLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDto user;
        if (!auth.getName().equals("anonymousUser")) {
            User currentUser = userService.findByUserName(auth.getName());
            user = userService.convertToDto(currentUser);
        } else {
            user = new UserDto();
            user.setEmail("anonymousUser");
        }
        return user;
    }


    @Override
    public String encodePassword(String password){
        return passwordEncoder.encode(password);
    }


}
