package com.helpdesk.service.security;


import com.helpdesk.dto.UserDto;

public interface SecurityService {

    void autoLogin(String email, String password);
    String encodePassword(String password);
    UserDto getLoggedInUser();
}
