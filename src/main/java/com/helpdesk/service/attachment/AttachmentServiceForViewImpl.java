package com.helpdesk.service.attachment;

import com.helpdesk.dto.AttachmentDto;
import com.helpdesk.entities.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AttachmentServiceForViewImpl implements AttachmentServiceForView {

    @Autowired
    AttachmentService attachmentService;


    @Override
    public int checkFileAttachment(int id) {

        List<Attachment> att = attachmentService.getAttachmentByTicketId(id);
        return att.size();
    }

    @Override
    public void uploadAttachment(MultipartFile file, int ticketId) {
        attachmentService.saveAttachment(file, ticketId);
    }

    @Override
    public Attachment getAttachmentByTicketId(int ticketId) {
        return attachmentService.getAttachmentByTicketId(ticketId).get(0);
    }

    @Override
    public List<AttachmentDto> getAttachments(int ticketId) {
        return attachmentService.getAttachmentByTicketId(ticketId).stream()
                .map(file -> new AttachmentDto(file.getId(), file.getName())).collect(Collectors.toList());
    }
}
