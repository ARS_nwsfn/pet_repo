package com.helpdesk.service.attachment;

import com.helpdesk.dao.AttachmentDao;
import com.helpdesk.entities.Attachment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    private static final Logger logger = Logger.getLogger(AttachmentServiceImpl.class);

    @Autowired
    AttachmentDao attachmentDao;

    @Override
    public void saveAttachment(Attachment att) {
        attachmentDao.save(att);
    }

    @Override
    public void saveAttachment(MultipartFile file, int id) {
        if (file.getSize() == 0 && file.getName().isEmpty()) return;
        Attachment att = new Attachment();
        try {
            att.setContent(file.getBytes());
        } catch (IOException e) {
            logger.error("couldn't store file bytes " + e.getMessage());
        }
        att.setName(file.getOriginalFilename());
        att.setType(file.getContentType());
        att.setTicketId(id);
        attachmentDao.save(att);
    }

    @Override
    public void saveAttachment(List<MultipartFile> files, int ticketId) {
        files.forEach(file -> saveAttachment(file, ticketId));
    }

    @Override
    public List<Attachment> getAttachmentByTicketId(int id) {
        return attachmentDao.getByTicketId(id);
    }

    @Override
    public void deleteAttachment(Attachment attachment) {
        attachmentDao.delete(attachment);
    }

    @Override
    public Attachment getAttachment(int attId) {
        return attachmentDao.getById(attId);
    }

    @Override
    public void deleteAttachment(int attId) {
        attachmentDao.delete(getAttachment(attId));
    }
}
