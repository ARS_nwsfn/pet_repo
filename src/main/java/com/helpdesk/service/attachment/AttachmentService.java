package com.helpdesk.service.attachment;

import com.helpdesk.entities.Attachment;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AttachmentService {

    void saveAttachment(Attachment att);
    void saveAttachment(MultipartFile file, int id);
    void saveAttachment(List<MultipartFile> files, int ticketId);
    List<Attachment> getAttachmentByTicketId(int id);
    void deleteAttachment(Attachment attachment);
    Attachment getAttachment(int attId);
    void deleteAttachment(int attId);
}
