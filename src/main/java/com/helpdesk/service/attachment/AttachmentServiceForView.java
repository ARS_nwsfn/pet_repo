package com.helpdesk.service.attachment;

import com.helpdesk.dto.AttachmentDto;
import com.helpdesk.entities.Attachment;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AttachmentServiceForView {

    int checkFileAttachment(int id);
    void uploadAttachment(MultipartFile file, int ticketId);
    Attachment getAttachmentByTicketId(int ticketId);
    List<AttachmentDto> getAttachments(int ticketId);
}
