package com.helpdesk.service.comment;

import com.helpdesk.dto.CommentDto;
import com.helpdesk.entities.Comment;
import com.helpdesk.service.security.SecurityService;
import com.helpdesk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceForViewImpl implements CommentServiceForView{

    @Autowired
    private UserService userService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private SecurityService securityService;

    @Override
    public List<CommentDto> getCommentsByTicketId(int id, int amount) {
        List<Comment> comments;
        if (amount == 0) {
            comments = commentService.getCommentByTicketId(id);
        } else {
            comments = commentService.getLimitedAmount(commentService.getCommentByTicketId(id), amount);
        }
        return comments.stream()
                .map(comment -> new CommentDto(comment.getId(),
                        userService.getUserById(comment.getUserId()).getFirstName() + " " + userService.getUserById(comment.getUserId()).getLastName(),
                        comment.getText(),
                        comment.getDate(),
                        comment.getTicketId()))
                .collect(Collectors.toList());
    }

    @Override
    public void saveComment(int ticketId, String text) {
        Comment comment = new Comment();
        comment.setTicketId(ticketId);
        comment.setText(text);
        comment.setUserId(securityService.getLoggedInUser().getId());
        comment.setDate(Calendar.getInstance());
        commentService.saveComment(comment);
    }
}
