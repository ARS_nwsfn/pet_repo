package com.helpdesk.service.comment;

import com.helpdesk.entities.Comment;

import java.util.List;

public interface CommentService {

    void saveComment(Comment comment);

    void saveComment(String text, int ticketId, int userId);

    List<Comment> getCommentByTicketId(int id);

    List<Comment> getLimitedAmount(List<Comment> comments, int amount);
}
