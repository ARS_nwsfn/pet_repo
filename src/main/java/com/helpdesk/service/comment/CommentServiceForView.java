package com.helpdesk.service.comment;

import com.helpdesk.dto.CommentDto;

import java.util.List;

public interface CommentServiceForView {

    List<CommentDto> getCommentsByTicketId(int id, int amount);
    void saveComment(int ticketId, String text);
}
