package com.helpdesk.service.comment;

import com.helpdesk.dao.CommentDao;
import com.helpdesk.entities.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Override
    public void saveComment(Comment comment) {
        commentDao.save(comment);
    }

    @Override
    public void saveComment(String text, int ticketId, int userId) {
        Comment comment = new Comment();
        comment.setText(text);
        comment.setTicketId(ticketId);
        comment.setUserId(userId);
        comment.setDate(Calendar.getInstance());
        commentDao.save(comment);
    }

    @Override
    public List<Comment> getCommentByTicketId(int id) {
        return commentDao.getByTicketId(id);
    }

    @Override
    public List<Comment> getLimitedAmount(List<Comment> comments, int amount) {
        comments.sort(Comparator.comparing(Comment::getDate));
        return comments.stream().skip(Math.max(0, comments.size() - amount)).collect(Collectors.toList());
    }
}
