package com.helpdesk.service.history;

import com.helpdesk.dto.HistoryDto;

import java.util.List;

public interface HistoryServiceForView {

    List<HistoryDto> getHistoryByTicketId(int id, int amount);

    void addHistory(int ticketId, String action, String description);
}
