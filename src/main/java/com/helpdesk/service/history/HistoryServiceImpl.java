package com.helpdesk.service.history;

import com.helpdesk.dao.HistoryDao;
import com.helpdesk.entities.History;
import com.helpdesk.service.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryDao historyDao;

    @Autowired
    private SecurityService securityService;

    @Override
    public void saveHistory(History history) {
        historyDao.save(history);
    }

    @Override
    public List<History> getHistoryByTicketId(int id) {
        return historyDao.getByTicketId(id);
    }

    @Override
    public List<History> getLimitedAmount(List<History> histories, int amount) {

        histories.sort(Comparator.comparing(History::getDate));
        return histories.stream().skip(Math.max(0, histories.size() - amount)).collect(Collectors.toList());
    }

    @Override
    public void addHistory(int ticketId, String action, String description) {
        History history = new History();
        history.setTicketId(ticketId);
        history.setDate(Calendar.getInstance());
        history.setAction(action);
        history.setUserId(securityService.getLoggedInUser().getId());
        history.setDescription(description);
        saveHistory(history);
    }
}
