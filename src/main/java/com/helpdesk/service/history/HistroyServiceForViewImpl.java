package com.helpdesk.service.history;

import com.helpdesk.dto.HistoryDto;
import com.helpdesk.entities.History;
import com.helpdesk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HistroyServiceForViewImpl implements HistoryServiceForView {

    @Autowired
    private HistoryService historyService;

    @Autowired
    private UserService userService;

    @Override
    public List<HistoryDto> getHistoryByTicketId(int id, int amount) {
        List<History> histories;
        if (amount == 0) {
            histories = historyService.getHistoryByTicketId(id);
        } else {
            histories = historyService.getLimitedAmount(historyService.getHistoryByTicketId(id), amount);
        }
        return histories.stream()
                .map(history -> new HistoryDto(history.getId(),
                        history.getTicketId(),
                        history.getDate(),
                        history.getAction(),
                        userService.getUserById(history.getUserId()).getFirstName() + " " + userService.getUserById(history.getUserId()).getLastName(),
                        history.getDescription()))
                .collect(Collectors.toList());
    }

    @Override
    public void addHistory(int ticketId, String action, String description) {
        historyService.addHistory(ticketId,action,description);
    }
}
