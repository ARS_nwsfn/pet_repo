package com.helpdesk.service.history;

import com.helpdesk.entities.History;

import java.util.List;

public interface HistoryService {

    void saveHistory(History history);

    List<History> getHistoryByTicketId(int id);

    List<History> getLimitedAmount(List<History> histories, int amount);

    void addHistory(int ticketId, String action, String description);
}
