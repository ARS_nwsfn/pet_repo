package com.helpdesk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = {"com.helpdesk"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().antMatchers("/ticket/create")
                .access("hasAnyAuthority('MANAGER','EMPLOYEE')")
                .and().authorizeRequests().antMatchers("/api/**","/ticket/*")
                .access("isAuthenticated()")
                .and().formLogin().loginProcessingUrl("/login").loginPage("http://localhost:3000/login").failureUrl("http://localhost:3000/login?error=true")
                .and().logout().logoutSuccessUrl("http://localhost:3000/login?logout=true")
                .and().formLogin().defaultSuccessUrl("http://localhost:3000/tickets", true)
                .usernameParameter("email")
                .passwordParameter("password")
                .and().csrf().disable();
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}