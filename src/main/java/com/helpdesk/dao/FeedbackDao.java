package com.helpdesk.dao;

import com.helpdesk.entities.Feedback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class FeedbackDao extends CrudDao<Feedback> {

    public FeedbackDao() {super(Feedback.class); }

    @Transactional
    public List<Feedback> getByTicketId(int id){

        String hql = "select f from Feedback f where f.ticketId = :id";
        TypedQuery<Feedback> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id);
        return  query.getResultList();
    }
}
