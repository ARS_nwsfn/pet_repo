package com.helpdesk.dao;

import com.helpdesk.entities.History;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class HistoryDao extends CrudDao<History> {

    public HistoryDao() {super(History.class); }

    @Transactional
    public List<History> getByTicketId(int id){

        String hql = "select h from History h where h.ticketId = :id";
        TypedQuery<History> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id);
        return  query.getResultList();
    }
}
