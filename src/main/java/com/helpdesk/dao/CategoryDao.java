package com.helpdesk.dao;

import com.helpdesk.entities.Category;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDao extends CrudDao<Category> {

    public CategoryDao(){ super(Category.class);}
}
