package com.helpdesk.dao;

import com.helpdesk.entities.Attachment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class AttachmentDao extends CrudDao<Attachment> {

public AttachmentDao() {super(Attachment.class); }

    @Transactional
    public List<Attachment> getByTicketId(int id){

        String hql = "select c from Attachment c where c.ticketId = :id";
        TypedQuery<Attachment> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id);
        return  query.getResultList();
    }
}
