package com.helpdesk.dao;

import com.helpdesk.entities.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserDao extends CrudDao<User> {

    public UserDao() {super(User.class); }

    @Transactional
    public User findByEmail (String email) { return sessionFactory.getCurrentSession().byNaturalId(User.class)
            .using("email",email)
            .load();}
}