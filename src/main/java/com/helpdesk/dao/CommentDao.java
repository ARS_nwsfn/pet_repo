package com.helpdesk.dao;

import com.helpdesk.entities.Comment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class CommentDao extends CrudDao<Comment> {
    public CommentDao() {super(Comment.class); }

    @Transactional
    public List<Comment> getByTicketId(int id){

        String hql = "select c from Comment c where c.ticketId = :id";
        TypedQuery<Comment> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id);
        return  query.getResultList();
    }
}
