package com.helpdesk.dao;


import com.helpdesk.entities.Ticket;
import com.helpdesk.entities.enums.State;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TicketDao extends CrudDao<Ticket> {

    public TicketDao() {super(Ticket.class); }

    @Transactional
    public List<Ticket> getByUserId(int id){

        String hql = "select t from Ticket t where t.ownerId = :searchId";
        TypedQuery<Ticket> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("searchId", id);
        return  query.getResultList();
    }

    @Transactional
    public List<Ticket> getByState(State stateSearch){

        String hql = "select t from Ticket t where t.state = :stateSearch";
        TypedQuery<Ticket> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("stateSearch",stateSearch);
        return  query.getResultList();
    }

    @Transactional
    public List<Ticket> getByApproverId(int id){

        String hql = "select t from Ticket t where t.approverId = :id";
        TypedQuery<Ticket> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id);
        return  query.getResultList();
    }

    @Transactional
    public List<Ticket> getByAssigneeId(int id){

        String hql = "select t from Ticket t where t.assigneeId = :id";
        TypedQuery<Ticket> query = sessionFactory.getCurrentSession().createQuery(hql)
                .setParameter("id", id);
        return  query.getResultList();
    }



}
