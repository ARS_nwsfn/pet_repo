package com.helpdesk.onstartup;

import com.helpdesk.entities.User;
import com.helpdesk.entities.enums.Role;
import com.helpdesk.service.user.UserService;
import com.helpdesk.service.user.UserUtilService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class RunJob implements InitializingBean{

    @Autowired
    private UserService userService;


    public void afterPropertiesSet() throws ParseException {

        // user 1
        User user1 = new User();
        user1.setEmail("user1_mogilev@yopmail.com");
        user1.setPassword(UserUtilService.encodePassword("123"));
        user1.setRole(Role.EMPLOYEE);
        user1.setFirstName("first");
        user1.setLastName("user");
        userService.saveUser(user1);

        // user 2
        User user2 = new User();
        user2.setEmail("user2_mogilev@yopmail.com");
        user2.setPassword(UserUtilService.encodePassword("123"));
        user2.setRole(Role.EMPLOYEE);
        user2.setFirstName("second");
        user2.setLastName("user");
        userService.saveUser(user2);

        // manager 1
        User manager1 = new User();
        manager1.setEmail("manager1_mogilev@yopmail.com");
        manager1.setPassword(UserUtilService.encodePassword("123"));
        manager1.setRole(Role.MANAGER);
        manager1.setFirstName("first ");
        manager1.setLastName("manager");
        userService.saveUser(manager1);

        // manager 2
        User manager2 = new User();
        manager2.setEmail("manager2_mogilev@yopmail.com");
        manager2.setPassword(UserUtilService.encodePassword("123"));
        manager2.setRole(Role.MANAGER);
        manager2.setFirstName("second ");
        manager2.setLastName("manager");
        userService.saveUser(manager2);

        // engineer 1
        User engineer1 = new User();
        engineer1.setEmail("engineer1_mogilev@yopmail.com");
        engineer1.setPassword(UserUtilService.encodePassword("123"));
        engineer1.setRole(Role.ENGINEER);
        engineer1.setFirstName("first  ");
        engineer1.setLastName("engineer");
        userService.saveUser(engineer1);

        // engineer 2
        User engineer2 = new User();
        engineer2.setEmail("engineer2_mogilev@yopmail.com");
        engineer2.setPassword(UserUtilService.encodePassword("123"));
        engineer2.setRole(Role.ENGINEER);
        engineer2.setFirstName("second  ");
        engineer2.setLastName("engineer");
        userService.saveUser(engineer2);

    }
}
